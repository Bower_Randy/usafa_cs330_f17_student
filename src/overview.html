<body>
  <p><span style="font-size: 133%; font-weight:bold;">CS 330 &ndash; Software Design &amp; Development</span>.</p>

  <p>Welcome to Computer Science 330, Software Design & Development. This document will serve as your syllabus (aka, course letter) and be the point of reference for administrative information and policies.</p>

  <p>Thus far in your programming experiences here at USAFA, you have been introduced to programming fundamentals in CS 210 using Python and explored nitty gritty details of data structures in CS 220 using C. This course will use the Java programming language and focus on higher-level, more general programming language concepts and in particular, the object-oriented programming paradigm.</p>

  <p>While this course is limited to a single period, there will still be daily lab exercises. <strong><em>To make this work, it is essential that you complete the assigned readings prior to class time.</em></strong> This will allow us to start class by reviewing results of the prior lesson, addressing any questions you have on the current lesson’s reading, and then quickly diving into the day’s lab activities. In addition to the labs, there will be graded homework exercises, programming exercises, and exams.</p>

  <p>I believe you will find CS 330 to be an enjoyable and educational experience!</p>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Course Goal and Outcomes</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor">
        <td>The goal of CS 330 is to provide students a deep understanding of the object-oriented programming paradigm and its application in software design and development. Introduce the functional programming paradigm and distinguish the merits of different programming paradigms and language design features.</td>
      </tr>
      <tr class="rowColor">
        <td>Upon completion of this course, cadets shall be able to:
          <ol>
            <li>Explain fundamental concepts of programming language design.</li>
            <li>Demonstrate proficiency with the object-oriented programming paradigm and its application in software design and development.</li>
            <li>Utilize the functional programming paradigm to solve a suitable problem of moderate complexity.</li>
          </ol>
        </td>
      </tr>
    </tbody>
  </table>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Course Information</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor"><td style="width:25%">Prerequisite</td> <td>Successful completion of CS 220, Data Abstraction, with a C or better.</td></tr>
      <tr class="rowColor"><td>Textbook</td> <td><em>Head First Java</em>, 2<sup>nd</sup> Edition, O'Reilly Media, 2005.</td></tr>
      <tr class="altColor"><td>Computers</td> <td>Cadets will need to bring their computer to every class meeting throughout the semester, with a functioning wireless connection, and preferably fully charged.</td></tr>
      <tr class="rowColor"><td>Readings</td> <td>There will be assigned reading for most lessons, some from the textbook and some online resources. Cadets are expected to do this reading <em>before</em> the lesson during which it will be discussed.</td></tr>
      <tr class="altColor"><td>Graded Events</td> <td>There will be graded homework assignments worth 100 points, programming exercises worth 400 points, graded reviews worth 250 points, and a final exam worth 250 points.</td></tr>
      <tr class="rowColor"><td>Final Exam Validation</td> <td>The top 5% of students in the course will validate the final exam.</td></tr>
      <tr class="altColor"><td>Final Grades</td> <td>A standard grade distribution is expected, with grade lines at 90%, 80%, 70%, and 60%, and +/- grades on the top and bottom of each category. Depending on final scores, there may be a slight curve, as appropriate.</td></tr>
      <tr class="rowColor"><td>Extra Credit</td> <td>Up to 5% extra credit may be awarded at the discretion of the course director.</td></tr>
    </tbody>
  </table>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Graded Work</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor"><td style="width:25%">DF Policies</td> <td>All cadets will be held to the standards outlined in the DF Academic Policies, Academics with Honor, Classroom Standards, and Cadet Standards policies. Locate and read these documents carefully.</td></tr>
      <tr class="rowColor"><td>DFCS Policies</td> <td>Additionally, all cadets will be held to the standards outlined in the DFCS Academic Policies. Locate and read this document carefully.</td></tr>
      <tr class="altColor"><td>Documentation Statement</td> <td>Each graded event must include a documentation statement. See Documentation section below for further details.</td></tr>
      <tr class="rowColor"><td>Due Date/Time</td> <td>The due date and time for each graded event will be specified in the assignment materials. In general, this will be either the beginning of class time, end of class time, or a specific time on the day of the lesson specified.</td></tr>
      <tr class="altColor"><td>Late Penalties</td> <td>Late penalties accrue at a rate of 25% for each 24-hour period (including non-class days) past the on-time turn-in date and time. The late penalty is a cap on the maximum grade that may be awarded for the late work. Thus, zero points will be awarded for work more than 72 hours late.</td></tr>
      <tr class="rowColor"><td>Submission Requirements</td> <td>All work will be submitted electronically with submission requirements specified in the assignment materials. Failure to follow these submission requirements may result in up to a 10% penalty and possibly require re-submission.</td></tr>
      <tr class="altColor"><td>Programming Standards</td> <td>All code submitted is expected to meet the specified Programming Standards. Code that does not meet these standards may be penalized up to 10% of the points possible on the assignment.</td></tr>
    </tbody>
  </table>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Help Policy and Documentation</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor"><td style="width:25%">Violation</td> <td>DFCS will recommend a course grade of F for any cadet who egregiously violates a graded event's help policy or contributes to a violation by others.</td></tr>
      <tr class="rowColor"><td>Authorized Resources</td> <td>The authorized resources for each graded event may vary, so you will be expected to carefully read the help policy of each.</td></tr>
      <tr class="altColor"><td>Documentation Statement</td> <td>Each graded event must include a documentation statement. Cadets must document all help received on graded events from sources other than their instructor or instructor-provided materials. Help from the instructor and instructor-provided materials (including the textbook) <strong><em>should not</em></strong> be documented; all other help, including Internet resources, must be documented.</td></tr>
      <tr class="rowColor"><td>Location</td> <td>Each graded event will specify how the documentation statement is to be included. In general, for programming exercises this will be as a comment at the beginning of the main file.</td></tr>
      <tr class="altColor"><td>Grading</td> <td>The documentation statement will be the first thing read by the instructor when grading. If the documentation statement is incomplete or vague, cadets will be required to re-document and re-submit the assignment, resulting in a 5% penalty and possibly late submission penalties.</td></tr>
      <tr class="rowColor"><td>Contents</td> <td>The documentation statement must explicitly describe <strong><em>what</em></strong> assistance was provided, <strong><em>where</em></strong> on the assignment the assistance was provided, and <strong><em>who</em></strong> provided the assistance. If no help was received on a graded event, the documentation statement must explicitly say so.</td></tr>
    </tbody>
  </table>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Sample Documentation Statements</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor"><td style="width:25%">Bad Example</td> <td>Cadet McFly explained how the flux capacitor worked.</td></tr>
      <tr class="rowColor"><td>Good Example</td> <td>Cadet McFly explained how the flux capacitor worked conceptually, using diagrams and the assignment materials. He did not look at my code nor did I look at his code during this discussion.<br /><br />
        <strong>Note:</strong> A situation such as this should result in full credit being earned as there is no violation of the honor code. However, such documentation is essential as it would explain any accidental code similarities.
      </td></tr>
      <tr class="altColor"><td>Bad Example</td> <td>Cadet McFly helped fix my <code>getFlux()</code> method.</td></tr>
      <tr class="rowColor"><td>Good Example</td> <td>Cadet McFly helped fix my <code>getFlux()</code> method by looking at my code and finding that I had <code>currentFlux < maxFlux</code> instead of <code>currentFlux <= maxFlux</code> on line 88.<br /><br />
        <strong>Note:</strong> A situation such as this may or may not result in less than full credit for the <code>getFlux()</code> method, but due to the documentation statement there is no violation of the honor code.
      </td></tr>
      <tr class="altColor"><td>Bad Example</td> <td>Cadet McFly and I worked together on the <code>getFlux()</code> method.</td></tr>
      <tr class="rowColor"><td>Good Example</td> <td>Cadet McFly and I worked together on the <code>getFlux()</code> method, each contributing equally to its development. Prior to this help, neither of our own <code>getFlux()</code> methods were working. My <code>getFlux()</code> method is now nearly identical to Cadet McFly's <code>getFlux()</code> method.<br /><br />
        <strong>Note:</strong> A situation such as this may or may not result in half credit being earned for the <code>getFlux()</code> method, but due to the documentation statement there is no violation of the honor code.
      </td></tr>
      <tr class="altColor"><td>Bad Example</td> <td>Cadet McFly showed me how the <code>getFlux()</code> method works.</td></tr>
      <tr class="rowColor"><td>Good Example</td> <td>Cadet McFly showed me how the <code>getFlux()</code> method works by letting me look at his code. Prior to this help my own <code>getFlux()</code> method was not working. My <code>getFlux()</code> method is now nearly identical to Cadet McFly's <code>getFlux()</code> method.<br /><br />
        <strong>Note:</strong> In a situation such as this, points may or may not be earned for the <code>getFlux()</code> method, but due to the documentation statement there is no violation of the honor code.
      </td></tr>
      <tr class="altColor"><td>Bad Example</td> <td>Cadet McFly showed me how the <code>getFlux()</code> method works.</td></tr>
      <tr class="rowColor"><td>Good Example</td> <td>Cadet McFly showed me how the <code>getFlux()</code> method works by looking at my code and talking me through each line as I wrote it. Prior to this help my own <code>getFlux()</code> method was wrong. My <code>getFlux()</code> method is now nearly identical to Cadet McFly's <code>getFlux()</code> method.<br /><br />
        <strong>Note:</strong> In a situation such as this, points may or may not be earned for the <code>getFlux()</code> method, but due to the documentation statement there is no violation of the honor code.
      </td></tr>
    </tbody>
  </table>

  <p><strong>Note:</strong> Each of the sample documentation statements above are written from the perspective of the cadet <strong><em>receiving</em></strong> the help. In each case, Cadet McFly should also document <strong><em>providing</em></strong> the help.</p>

  <p>&nbsp;</p>

  <table class="overviewSummary" border="1" style="border-collapse:collapse; width:100%;">
    <caption><span>Expectations</span><span class="tabEnd">&nbsp;</span></caption>
    <tbody>
      <tr class="altColor"><td style="width:25%">Time Management</td> <td>Cadets are expected to begin work on programming exercises well in advance of the due date. These exercises are not designed to be completed in one marathon programming session; plan and budget your time wisely.</td></tr>
      <tr class="rowColor"><td>Preparation</td> <td>Cadets are expected to come to class having completed the reading and/or any other preparation (e.g., watch a relevant video) specified on the course schedule or by the instructor.</td></tr>
      <tr class="altColor"><td>Engagement</td> <td>Cadets are expected to be actively engaged with the material, the instructor, and their classmates during each lesson, as appropriate. This precludes working on other courses, using cell phones or the Internet for non-class related activities, or otherwise distracting yourself or other cadets.</td></tr>
      <tr class="rowColor"><td>Resourcefulness</td> <td>Cadets are expected to demonstrate a basic level of resourcefulness and initiative during the semester. For example, it is expected that cadets carefully read a programming exercise specification document to determine such things as the required functionality and implementation strategy.</td></tr>
      <tr class="altColor"><td>Seek Assistance</td> <td>While remaining resourceful and continuing to take initiative, cadets are expected to seek assistance in a timely manner when necessary. Your instructor is fully committed to helping each cadet be successful; take full advantage of this resource.</td></tr>
      <tr class="rowColor"><td>Respect</td> <td>Cadets are expected to behave with a level of professionalism, commitment, and respect commensurate with their position as a cadet at the United States Air Force Academy and their goal of becoming an officer in the United States Air Force.</td></tr>
    </tbody>
  </table>

  <p>&nbsp;</p>

</body>
