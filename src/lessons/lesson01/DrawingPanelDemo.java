package lessons.lesson01;

import utils.DrawingPanel;

import java.awt.*;

/**
 * Demonstrate some simple drawing with the DrawingPanel class.
 *
 * @author Dr. Randy Bower
 */
public class DrawingPanelDemo
{
    /** Constant for the width of the drawing panel. */
    private static final int WIDTH = 800;
    /** Constant for the height of the drawing panel; based on width to produce the visually pleasing 4:3 ratio. */
    private static final int HEIGHT = WIDTH * 3 / 4;

    public static void main( String[] args )
    {
        // Create a drawing panel and get its graphics object.
        DrawingPanel panel = new DrawingPanel( WIDTH, HEIGHT );
        Graphics2D pen = panel.getGraphics();

        // Set a few properties of the drawing panel.
        panel.setWindowTitle( "Demo" );
        panel.setBackground( Color.CYAN );

        // Set a few properties of the pen.
        pen.setColor( Color.BLACK );
        pen.setFont( new Font( Font.SERIF, Font.BOLD, 32 ) );

        // Draw three smiling smileys.
        drawSmiley( pen, WIDTH / 2 - 50, HEIGHT / 2 - 100, 100, 200 );  // Center smiley.
        drawSmiley( pen, WIDTH / 4 - 25, HEIGHT / 2 - 50, 50, 100 );  // Left smiley.
        drawSmiley( pen, WIDTH * 3 / 4 - 25, HEIGHT / 2 - 50, 100, 100 );  // Right smiley.

        // Write a message on the bottom of the window, centered horizontally.
        String msg = "Hello, World!";
        pen.drawString( msg,
                WIDTH / 2 - pen.getFontMetrics().stringWidth( msg ) / 2,
                HEIGHT * 95 / 100 );

        // The drawing panel uses a technique called "double buffering" where all drawing is done
        // in an off-screen buffer and is only copied to the screen when this method is called.
        // The result is much smoother graphics, especially during an animation.
        panel.copyGraphicsToScreen();

        // Wait for a mouse click and then close the window.
        panel.waitForMouseClick( DrawingPanel.LEFT_BUTTON );
        panel.closeWindow();
    }

    /**
     * Draws a smiley within the specified bounding rectangle. The head will be in
     * the top half of the rectangle and the body in the lower half.
     *
     * @param pen The Graphics2D object to do the drawing.
     * @param x The x-coordinate of the upper-left corner of the bounding rectangle.
     * @param y The y-coordinate of the upper-left corner of the bounding rectangle.
     * @param w The width of the bounding rectangle.
     * @param h The height of the bounding rectangle.
     */
    private static void drawSmiley( Graphics2D pen, int x, int y, int w, int h )
    {
        // Draw the head in yellow and outline in black.
        pen.setColor( Color.YELLOW );
        pen.fillOval( x, y, w, h / 2 );
        pen.setColor( Color.BLACK );
        pen.drawOval( x, y, w, h / 2 );

        // Draw the smile by drawing a black circle and then
        // an offset yellow circle to leave a crescent of black.
        pen.fillOval( x + w / 5, y + h / 10, w * 3 / 5, h * 3 / 10 );
        pen.setColor( Color.YELLOW );
        pen.fillOval( x + w / 5, y + h / 15, w * 3 / 5, h * 3 / 10 );

        // Draw the nose and eyes.
        pen.setColor( Color.BLACK );
        pen.fillOval( x + w / 2 - w / 12, y + h / 4 - h / 24, w / 6, h / 12 );  // Nose.
        pen.fillOval( x + w / 4, y + h / 8, w / 6, h / 12 );  // Left eye.
        pen.fillOval( x + w * 3 / 4 - w / 6, y + h / 8, w / 6, h / 12 );  // Right eye.

        // Draw the body, arms, and legs.
        pen.drawLine( x + w / 2, y + h / 2, x + w / 2, y + h * 7 / 8 );  // Body.
        pen.drawLine( x + w / 2, y + h * 7 / 8, x + w / 3, y + h );  // Left leg.
        pen.drawLine( x + w / 2, y + h * 7 / 8, x + w * 2 / 3, y + h );  // Right leg.
        pen.drawLine( x + w / 2, y + h * 5 / 8, x + w / 4, y + h * 9 / 16 );  // Left arm.
        pen.drawLine( x + w / 2, y + h * 5 / 8, x + w * 3 / 4, y + h * 9 / 16 );  // Right arm.
    }
}
