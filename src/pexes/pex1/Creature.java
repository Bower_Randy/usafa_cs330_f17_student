package pexes.pex1;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Abstract base class for all creatures in the aquarium.
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Creature
{
    // **** NOTE: For this programming exercise, all class attributes
    // **** for all creatures must be declared as private.

    /**
     * Constructor creates a new creature with initial properties set to
     * random values within the bounds of the aquarium; different types of
     * creatures may impose further restrictions on these values.
     */
    public Creature()
    {
    }

    /**
     * Move this creature.
     */
    public void move()
    {
    }

    /**
     * React to meeting another creature.
     */
    public void react()
    {
    }

    /**
     * Determine if this creature has met the given creature.
     *
     * @param that The creature which has possibly met this creature.
     *
     * @return true if the two creatures have met; false otherwise.
     */
    public boolean met( Creature that )
    {
        return false;
    }

    /**
     * Paints the creature's image with the given graphics object.
     *
     * @param g The Graphics object to use to paint this creature.
     */
    public void paint( Graphics g )
    {
    }

    /**
     * Sets the x position of this creature, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * creature. Creatures are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the creature as well as the aquarium's dimensions.
     *
     * @param x New x position for this creature.
     */
    public void setX( double x )
    {
    }

    /**
     * Sets the y position of this creature, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * creature. Creatures are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the creature as well as the aquarium's dimensions.
     *
     * @param y New y position for this creature.
     */
    public void setY( double y )
    {
    }

    /**
     * Sets the z position of this creature, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * creature. Creatures are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the creature as well as the aquarium's dimensions.
     * <p>
     * <strong>Note</strong>: The depth of the aquarium is the distance
     * from the back to the front of the aquarium, not the water depth.
     * </p>
     *
     * @param z New z position for this creature.
     */
    public void setZ( double z )
    {
    }

    /**
     * Sets the speed of this creature, which must be between 0.5 and 2.0,
     * positive or negative.
     *
     * @param speed New speed position for this creature.
     *
     * @throws IllegalArgumentException If speed is out of range.
     */
    public void setSpeed( double speed )
    {
    }

    /**
     * Convenience method to set the location of this creature.
     *
     * @param x     New x position for this creature.
     * @param y     New y position for this creature.
     * @param z     New z position for this creature.
     */
    public void setXYZ( double x, double y, double z )
    {
    }

    /**
     * Accessor method for the x position of this creature.
     *
     * @return The current x position of this creature.
     */
    public double getX()
    {
        return 0.0;
    }

    /**
     * Accessor method for the y position of this creature.
     *
     * @return The current y position of this creature.
     */
    public double getY()
    {
        return 0.0;
    }

    /**
     * Accessor method for the z position of this creature.
     *
     * @return The current z position of this creature.
     */
    public double getZ()
    {
        return 0.0;
    }

    /**
     * Accessor method for the speed of this creature.
     *
     * @return The current speed of this creature.
     */
    public double getSpeed()
    {
        return 0.0;
    }

    /**
     * Accessor method for the scale of this creature.
     *
     * @return The current scale of this creature.
     */
    public double getScale()
    {
        return 0.0;
    }

    /**
     * Accessor method for the width of this creature. It is determined by
     * the current scale of the creature and the width of the creature's image.
     *
     * @return The current width of this creature.
     */
    public double getWidth()
    {
        return 0.0;
    }

    /**
     * Accessor method for the height of this creature. It is determined by
     * the current scale of the creature and the height of the creature's image.
     *
     * @return The current height of this creature.
     */
    public double getHeight()
    {
        return 0.0;
    }

    /**
     * Accessor method for the depth of this creature. It is determined by
     * the current scale of the creature and is somewhat arbitrarily set
     * to one-half of the height of the creature.
     * <p>
     * <strong>Note</strong>: The depth of a creature is its "thickness"
     * or the amount of space it occupies front to back in the aquarium.
     * </p>
     *
     * @return The current depth of this creature.
     */
    public double getDepth()
    {
        return 0.0;
    }

    /**
     * Creates and returns a string representation of this creature that
     * includes the position, size, scale, and speed of the creature.
     * Only used for debugging, so it's not very fancy.
     *
     * @return A string representation of this creature.
     */
    @Override
    public String toString()
    {
        return String.format( "%s (%.1f, %.1f, %.1f), (%.1f, %.1f, %.1f), %.1f, %.1f",
                this.getClass().getSimpleName(), this.getX(), this.getY(), this.getZ(),
                this.getWidth(), this.getHeight(), this.getDepth(), this.getScale(), this.getSpeed() );
    }
}
