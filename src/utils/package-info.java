/**
 * Various utility classes for simple graphics and file i/o.
 *
 * @author Dr. Randy Bower
 */
package utils;
