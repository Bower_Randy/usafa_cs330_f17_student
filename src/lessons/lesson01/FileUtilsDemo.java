package lessons.lesson01;

import utils.FileUtils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Demonstrate the utility methods in the FileUtils class.
 *
 * @author Dr. Randy Bower
 */
public class FileUtilsDemo
{
    /**
     * Launches the various demos.
     *
     * @param args Command line arguments; ignored by this application.
     */
    public static void main( String[] args )
    {
        selectAndCopyDemo();
        readFileDemos();
    }

    /** Demonstrates the selectInputFile and selectOutputFile methods. */
    private static void selectAndCopyDemo()
    {
        // Prompts the user to select an input file with a standard open file dialog box.
        Scanner input = FileUtils.selectInputFile( null );

        // The input scanner may be null if the user clicks the cancel button.
        if( input != null )
        {
            // Prompts the user to select an output file with a standard save file dialog box.
            PrintStream output = FileUtils.selectOutputFile( null );

            // The output print stream may be null if the user clicks the cancel button.
            if( output != null )
            {
                // Loop as long as the input file has another line of data.
                while( input.hasNextLine() )
                {
                    // Read the next line from the input scanner and
                    // immediately write to the output stream.
                    output.println( input.nextLine() );
                }

                // Always remember to close files; especially important for output files.
                input.close();
                output.close();
            }
        }
    }

    /** Demonstrates reading an entire file into a string or a list. */
    private static void readFileDemos()
    {
        // The file path is specified starting in the root of the project.
        System.out.println( FileUtils.readFileAsString( "./data/Captain.txt" ) );

        // If a file's contents need a bit more processing, it may be handy
        // to read the file into a list of individual lines of data.
        ArrayList<String> data = FileUtils.readFileAsList( "./data/Names.txt" );

        // If the file was not found or the user clicked Cancel on the file dialog.
        if( data != null )
        {
            // Use Java's "enhanced for loop" to process an list of strings.
            for( String name : data )
            {
                String[] names = name.split( " " );
                System.out.println( names[ 2 ] + ", " + names[ 0 ] + " " + names[ 1 ].charAt( 0 ) + "." );
            }
        }
    }
}
