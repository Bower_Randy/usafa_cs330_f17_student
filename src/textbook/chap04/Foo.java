package textbook.chap04;

public class Foo
{
    public void go()
    {
        int x = 0;  // Initialization added so example could be included in project.

        int z = x + 3;  // This will not compile without the initialization above.
    }
}
