package pexes.pex2;

/**
 * This class represents a chess board used in Shogi Chess.
 * <p>
 * The class contains a two-dimensional array of ChessPiece objects where the
 * ChessPiece object in 1,1 is in the lower-left corner of the board and the
 * ChessPiece object in ROWS,COLS is in the upper-right corner of the board.
 * <p><pre>
 *   _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
 *  |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
 *  | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
 *  | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
 *  | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
 *  |_____/_\____/  |_|_\_|\____/__|_| ___|_|_ |_|\____/|_____/_____|_|__     |_|
 *     |__   __| |  | |_   _|/ ____|  / ____| |        /\    / ____/ ____|
 *        | |  | |__| | | | | (___   | |    | |       /  \  | (___| (___
 *        | |  |  __  | | |  \___ \  | |    | |      / /\ \  \___ \\___ \
 *        | |  | |  | |_| |_ ____) | | |____| |____ / ____ \ ____) |___) |
 *        |_|  |_|  |_|_____|_____/   \_____|______/_/    \_\_____/_____/
 * </pre>
 *
 * @author Dr. Randy Bower
 */
public class ChessBoard
{
    /** A standard Shogi board is 9x9. */
    public static final int SIZE = 9;
    /** Constant to be used for ChessPiece.direction for pieces moving down. */
    public static final int DOWN = -1;
    /** Constant to be used for ChessPiece.direction for pieces moving up. */
    public static final int UP = 1;

    /** Names of the starting pieces on the board. */
    private static final String[] NAMES = {
            "King", "GoldGeneral", "GoldGeneral", "SilverGeneral", "SilverGeneral",
            "Knight", "Knight", "Lance", "Lance", "Bishop", "Rook" };
    /** Locations of the starting pieces moving down on the board. */
    private static final int[][] DOWN_PIECES = {
            { 9, 5 }, { 9, 4 }, { 9, 6 }, { 9, 3 }, { 9, 7 }, { 9, 2 },
            { 9, 8 }, { 9, 1 }, { 9, 9 }, { 8, 8 }, { 8, 2 } };
    /** Locations of the starting pieces moving up on the board. */
    private static final int[][] UP_PIECES = {
            { 1, 5 }, { 1, 4 }, { 1, 6 }, { 1, 3 }, { 1, 7 }, { 1, 2 },
            { 1, 8 }, { 1, 1 }, { 1, 9 }, { 2, 2 }, { 2, 8 } };

    /** Two-dimensional array of ChessPiece objects. */
    private ChessPiece[][] pieces;

    /**
     * Default constructor creates the array of chess piece object references.
     */
    public ChessBoard()
    {
        // Note: For logical simplicity, the lower-left corner is 1,1
        // and the upper-right corner is ROWS, COLS; index 0 is ignored.
        this.pieces = new ChessPiece[ SIZE + 1 ][ SIZE + 1 ];
    }

    /**
     * This method initializes the board with the starting configuration of chess pieces.
     * <p>
     * <strong>Note</strong>: This method is separated from the constructor so JUnit
     * tests can create a completely empty board with no chess pieces on it.
     */
    public void init()
    {
        // The following code uses the java.lang.reflect.Constructor class to create ChessPiece
        // objects from their class names represented as Strings. Doing this allows the GUI to
        // compile and run before students create all of the ChessPiece classes.
        String pieceName = "";
        // Loop through all of the non-Pawn piece names.
        for( int i = 0; i < NAMES.length; i++ )
        {
            try
            {
                // Put the package name in front of the chess piece class name.
                pieceName = this.getClass().getPackage().getName() + "." + NAMES[ i ];

                // Gets an array of constructors for the given chess piece class.
                // Each class should have exactly one constructor with takes three parameters.
                @SuppressWarnings( "unchecked" )
                java.lang.reflect.Constructor<ChessPiece>[] constructors =
                        (java.lang.reflect.Constructor<ChessPiece>[]) Class.forName( pieceName ).getConstructors();

                // Try to construct the up and down chess pieces.
                this.pieces[ DOWN_PIECES[ i ][ 0 ] ][ DOWN_PIECES[ i ][ 1 ] ] =
                        constructors[ 0 ].newInstance( DOWN, DOWN_PIECES[ i ][ 0 ], DOWN_PIECES[ i ][ 1 ] );
                this.pieces[ UP_PIECES[ i ][ 0 ] ][ UP_PIECES[ i ][ 1 ] ] =
                        constructors[ 0 ].newInstance( UP, UP_PIECES[ i ][ 0 ], UP_PIECES[ i ][ 1 ] );
            }
            catch( Exception e )
            {
                System.err.println( pieceName + " class was not found or could not be properly constructed." );
                System.err.println( "The " + pieceName + " class must contain exactly one constructor:" );
                System.err.println( "    public " + pieceName + "( int direction, int row, int col )" );
                System.err.println( "    {" );
                System.err.println( "        super( direction, row, col );" );
                System.err.println( "    }" );
                System.err.println();
            }
        }

        // Pawns are done separately because there are so many of them in the same row.
        try
        {
            pieceName = this.getClass().getPackage().getName() + ".Pawn";
            @SuppressWarnings( "unchecked" )
            java.lang.reflect.Constructor<ChessPiece>[] con = (java.lang.reflect.Constructor<ChessPiece>[]) Class.forName( pieceName ).getConstructors();
            for( int col = 1; col <= SIZE; col++ )
            {
                this.pieces[ 7 ][ col ] = con[ 0 ].newInstance( DOWN, 7, col );
                this.pieces[ 3 ][ col ] = con[ 0 ].newInstance( UP, 3, col );
            }
        }
        catch( Exception e )
        {
            System.err.println( pieceName + " class was not found or could not be properly constructed." );
            System.err.println( "The " + pieceName + " class must contain exactly one constructor:" );
            System.err.println( "  public " + pieceName + "( int direction, int row, int col )" );
            System.err.println( "  {" );
            System.err.println( "    super( direction, row, col );" );
            System.err.println( "  }" );
            System.err.println();
        }
    }

    /**
     * Places the ChessPiece object in the indicated row,col location of the chess board.
     * <p>
     * If there is currently a chess piece in the indicated location, it will be replaced.
     * <p>
     * Passing <code>null</code> as the value for the chess piece effectively removes whatever
     * chess piece may have been in that location.
     * <p>
     * <strong>Note</strong>: This method should only be called directly by JUnit tests;
     * during actual game play it is only called by the GUI.
     *
     * @param row   row for this chess piece.
     * @param col   column for this chess piece.
     * @param piece the ChessPiece object to place in this row,col location.
     *
     * @throws IllegalArgumentException if either row or col is not in range [1,ChessBoard.SIZE].
     */
    public void setPieceAt( int row, int col, ChessPiece piece )
    {
        if( row < 0 || row >= this.pieces.length )
        {
            throw new IllegalArgumentException( "ERROR: Row out of range." );
        }

        if( col < 0 || col >= this.pieces[ 0 ].length )
        {
            throw new IllegalArgumentException( "ERROR: Col out of range." );
        }

        this.pieces[ row ][ col ] = piece;
    }

    /**
     * Returns the ChessPiece object in the indicated row,col location from the chess board.
     * <p>
     * If there is not currently a chess piece in the indication location, it will return
     * <code>null</code>.
     *
     * @param row row for this chess piece.
     * @param col column for this chess piece.
     *
     * @return the ChessPiece object at location row,col; <code>null</code> if location is empty.
     *
     * @throws IllegalArgumentException if either row or col is not in range [1,ChessBoard.SIZE].
     */
    public ChessPiece getPieceAt( int row, int col )
    {
        if( row < 0 || row >= this.pieces.length )
        {
            throw new IllegalArgumentException( "ERROR: Row out of range." );
        }

        if( col < 0 || col >= this.pieces[ 0 ].length )
        {
            throw new IllegalArgumentException( "ERROR: Col out of range." );
        }

        return this.pieces[ row ][ col ];
    }
}
