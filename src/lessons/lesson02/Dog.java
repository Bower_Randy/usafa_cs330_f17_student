package lessons.lesson02;

/**
 * Dog example from chapter 2, page 36.
 *
 * @author M3
 */
public class Dog
{
    /** The size of this dog in pounds. */
    public int size;
    /** The breed of this dog such as "Labrador" or "Golden Retriever". */
    public String breed;
    /** The name of this dog such as "Scooby", "Snoopy", or "Petey". */
    public String name;

    /**
     * Creates a new Dog with default/unknown attribute values.
     */
    public Dog()
    {
        this( "Unnamed", "Unknown", 0 );
    }

    /**
     * Creates a new Dog with the given attribute values.
     *
     * @param name the name of this dog.
     * @param breed the breed of this dog.
     * @param size the size of this dog, in pounds.
     */
    public Dog( String name, String breed, int size )
    {
        this.name = name;
        this.breed = breed;
        this.size = size;
    }

    /**
     * Causes this dog to bark.
     */
    public void bark()
    {
        System.out.println( String.format( "%s says Ruff! Ruff!", this.name ) );
    }

    /**
     * Builds and returns a string representation of this dog.
     *
     * @return a string representation of this dog with name, size, and breed.
     */
    public String toString()
    {
        return String.format( "%s is a %d pound %s.", this.name, this.size, this.breed );
    }

    /**
     * Main function to test this class.
     *
     * @param args command line arguments; ignored in this application.
     */
    public static void main( String[] args )
    {
        // Creating a Dog object and individually setting each attribute is a hassle.
        Dog d = new Dog();
        d.name = "Rojo";
        d.breed = "Lab";
        d.size = 40;
        System.out.println( d );
        d.bark();

        // Creating a dog object with parameters to a constructor is handy.
        Dog t = new Dog( "Trouble", "Mutt", 65 );
        System.out.println( t );
        t.bark();
    }
}
