package lessons.lesson08;

/**
 * Cub class as part of the Cat hierarchy.
 *
 * @author Dr. Randy Bower
 */
public class Cub extends Lion
{
    /**
     * Construct a new cub with the given name.
     *
     * @param name the name of this cub.
     */
    public Cub( String name )
    {
        super( name );
    }

    /**
     * A string representation of a Cub has a little extra, in
     * addition to the information included about all cats.
     *
     * @return string representation of this Cub.
     */
    @Override
    public String toString()
    {
        // https://www.youtube.com/watch?v=xB5ceAruYrI
        return super.toString() + "\n  Cubs can also sing a little ditty called Hakuna Matata!";
    }
}
