package lessons.lesson08;

/**
 * Lion class as part of the Cat hierarchy.
 *
 * @author Dr. Randy Bower
 */
public class Lion extends Cat
{
    /**
     * Construct a new lion with the given name.
     *
     * @param name the name of this lion.
     */
    public Lion( String name )
    {
        super( name );
    }
}
