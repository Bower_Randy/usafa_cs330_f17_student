package lessons.lesson08;

/**
 * Driver program to demonstrate inheritance and polymorphism
 * with the Cat class hierarchy.
 *
 * TODO 1: Create a Tiger class such that tigers "walk" when they move and
 * TODO 1: say, "They're great!" when they make noise. In the main program,
 * TODO 1: add to the list of cats an instance of the Tiger class named Tony.
 * TODO 1: https://en.wikipedia.org/wiki/Tony_the_Tiger
 *
 * TODO 2: See comments at the bottom of this file for the second task.
 *
 * TODO 3: Override the getFood methods in the appropriate classes such that
 * TODO 3: Tony eats "Frosted Flakes" and Chester eats "Cheetos".
 * TODO 3: https://en.wikipedia.org/wiki/Chester_Cheetah
 *
 * TODO 4: Override the getNoise method in the appropriate class such that
 * TODO 4: Sylvester says "sufferin' succotash!".
 * TODO 4: https://www.youtube.com/watch?v=PkhPuH8G5Hg
 *
 * TODO 5: Add a new class of your choice that inherits from a class other
 * TODO 5: than Cat. Give your new class a unique behavior and then test
 * TODO 5: this in the main program.
 *
 * TODO 6: The toString method in the Cub class utilizes the super object
 * TODO 6: reference to build and return a string with the basic information
 * TODO 6: found in all Cat objects and also the information unique to a Cub.
 * TODO 6: Read, discuss, and understand this code.
 *
 * TODO 7: Modify or add an appropriate getNoise method so both Lions and Cubs
 * TODO 7: "purr and roar". Be sure to use the super object reference!
 * TODO 7: https://www.youtube.com/watch?v=nrLyllumxts
 *
 * TODO 8: Modify or add appropriate getMove methods such that Cheetahs
 * TODO 8: "sprint or saunter" and Cubs "saunter or romp" (note the order of
 * TODO 8: words in each of these.) Be sure to use the super object reference!
 * TODO 8: https://en.wikipedia.org/wiki/Cheetah#Speed_and_acceleration
 *
 * TODO 9: Modify the appropriate getFood method such that Sylvester will eat
 * TODO 9: "meat, like all cats, but prefers Tweedy Bird". Be sure to use the
 * TODO 9: super object reference! https://en.wikipedia.org/wiki/Tweety
 */
public class Main
{
    public static void main( String[] args )
    {
        java.util.ArrayList<Cat> cats = new java.util.ArrayList<>();
        cats.add( new Cat( "Felix" ) );
        cats.add( new Lion( "Mufasa" ) );
        cats.add( new Cub( "Simba" ) );
        cats.add( new Cheetah( "Chester" ) );
        cats.add( new Cartoon( "Sylvester" ) );
        // cats.add( new Tiger( "Tony" ) );

        for( Cat cat : cats )
        {
            System.out.println( cat );
        }

        // TODO 2a: Add the getFood method to your class hierarchy so all cats eat meat.
        // TODO 2a: https://en.wikipedia.org/wiki/Carnivore#Obligate_carnivores
        // TODO 2b: Un-comment the lines below to display what all cats eat.
        //for( Cat cat : cats )
        //{
        //    System.out.println( String.format( "%s eats %s.", cat.getName(), cat.getFood() ) );
        //}
    }
}
