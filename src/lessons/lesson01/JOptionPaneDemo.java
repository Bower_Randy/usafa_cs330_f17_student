package lessons.lesson01;

import javax.swing.JOptionPane;

/**
 * Demonstrate a few methods in the uber handy JOptionPane class.
 *
 * @author Dr. Randy Bower
 */
public class JOptionPaneDemo
{
    public static void main( String[] args )
    {
        String name = JOptionPane.showInputDialog( "What is your name?", "Clarice" );
        JOptionPane.showMessageDialog( null, "Hello, " + name + "." );

        int answer = Integer.parseInt( JOptionPane.showInputDialog( "What is the answer?", "42" ) );
        JOptionPane.showMessageDialog( null, "Correct, the answer is " + answer + "." );

        switch( JOptionPane.showConfirmDialog( null, "Click a button ... any button ..." ) )
        {
            case JOptionPane.YES_OPTION:
                JOptionPane.showMessageDialog( null, "Yes!",
                        "Yes ...", JOptionPane.WARNING_MESSAGE );
                break;
            case JOptionPane.NO_OPTION:
                JOptionPane.showMessageDialog( null, "No soup for you!",
                        "No ...", JOptionPane.ERROR_MESSAGE );
                break;
            case JOptionPane.CANCEL_OPTION:
                JOptionPane.showMessageDialog( null, "Really?",
                        "Cancel ...", JOptionPane.QUESTION_MESSAGE );
                break;
            default:
                // Should not happen in this example, but it's good practice to always include a default.
                JOptionPane.showMessageDialog( null, "Wait, what?",
                        "Default ...", JOptionPane.INFORMATION_MESSAGE );
                break;
        }

        String[] options = { "Daphne", "Fred", "Norville", "Velma" };
        int selection = JOptionPane.showOptionDialog( null,
                "Who is your favorite?", "Zoinks!",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
                null, options, options[ 0 ] );
        // Make sure the user didn't close the dialog without selecting one.
        if( selection >= 0 )
        {
            JOptionPane.showMessageDialog( null, "You like " + options[ selection ] + ".",
                    "You have chosen poorly.", JOptionPane.INFORMATION_MESSAGE );
        }

        String option = (String) JOptionPane.showInputDialog( null,
                "No, really, who is your favorite?", "Ruh Roh",
                JOptionPane.QUESTION_MESSAGE, null,
                options, options[ 0 ] );
        // Make sure the user didn't close the dialog without selecting one.
        if( option != null )
        {
            JOptionPane.showMessageDialog( null, "You like " + option + ".",
                    "You have chosen wisely.", JOptionPane.INFORMATION_MESSAGE );
        }
    }
}
