package pexes.pex2;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * GUI for Shogi Chess.
 * <p><pre>
 *   _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
 *  |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
 *  | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
 *  | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
 *  | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
 *  |_____/_\____/  |_|_\_|\____/__|_| ___|_|_ |_|\____/|_____/_____|_|__     |_|
 *     |__   __| |  | |_   _|/ ____|  / ____| |        /\    / ____/ ____|
 *        | |  | |__| | | | | (___   | |    | |       /  \  | (___| (___
 *        | |  |  __  | | |  \___ \  | |    | |      / /\ \  \___ \\___ \
 *        | |  | |  | |_| |_ ____) | | |____| |____ / ____ \ ____) |___) |
 *        |_|  |_|  |_|_____|_____/   \_____|______/_/    \_\_____/_____/
 * </pre>
 *
 * @author Dr. Randy Bower
 */
public class ShogiGUI
{
    /** Main application frame. */
    private JFrame frame;
    /** Red panel at top of GUI to indicate down player's turn. */
    private JPanel downTurnIndicator;
    /** Red panel at bottom of GUI to indicate up player's turn. */
    private JPanel upTurnIndicator;
    /** Main panel in center of GUI to view chess board. */
    private ChessBoardPanel boardPanel;

    /**
     * Creates and shows the main GUI window.
     */
    private ShogiGUI()
    {
        // Create the frame and set a few properties.
        this.frame = new JFrame( "Shogi Chess" );
        this.frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        this.frame.setLocation( 32, 32 );
        this.frame.setResizable( false );

        // Create the components to display the board and the turn indicators.
        this.boardPanel = new ChessBoardPanel( this );
        this.boardPanel.setLayout( null );
        this.downTurnIndicator = new JPanel( new FlowLayout() );
        this.upTurnIndicator = new JPanel( new FlowLayout() );
        this.downTurnIndicator.setPreferredSize(
                new Dimension( this.boardPanel.getWidth(), ChessBoardPanel.CELL_SIZE / 2 ) );
        this.upTurnIndicator.setPreferredSize(
                new Dimension( this.boardPanel.getWidth(), ChessBoardPanel.CELL_SIZE / 2 ) );

        // An inner panel with a blue background color is used so the
        // 4-pixel gaps in the BorderLayout will show through as blue.
        JPanel panel = new JPanel();
        panel.setLayout( new BorderLayout( 4, 4 ) );
        panel.setBackground( Color.BLUE );
        panel.add( this.downTurnIndicator, BorderLayout.NORTH );
        panel.add( this.boardPanel, BorderLayout.CENTER );
        panel.add( this.upTurnIndicator, BorderLayout.SOUTH );

        // Finish setting up the frame.
        this.frame.add( panel, BorderLayout.CENTER );
        this.frame.pack();

        // Sets one of the turn indicates to red, the other to blue.
        this.updateTurnIndicator();
    }

    /**
     * Sets the turn indicator (red panel on top or bottom of window).
     */
    private void updateTurnIndicator()
    {
        if( this.boardPanel.getTurn() == ChessBoard.DOWN )
        {
            this.downTurnIndicator.setBackground( Color.RED );
            this.upTurnIndicator.setBackground( Color.BLUE );
        }
        else
        {
            this.downTurnIndicator.setBackground( Color.BLUE );
            this.upTurnIndicator.setBackground( Color.RED );
        }
    }

    /**
     * Instantiates and displays a new Shogi GUI.
     */
    public static void launch()
    {
        // This fancy little tidbit is pretty standard code
        // for launching a GUI in a separate thread.
        java.awt.EventQueue.invokeLater(
                () ->
                {
                    try
                    {
                        ShogiGUI shogi = new ShogiGUI();
                        shogi.frame.setVisible( true );
                    }
                    catch( Exception e )
                    {
                        System.err.println( "ERROR: Could not launch Shogi GUI." );
                        e.printStackTrace();
                    }
                }
        );
    }

    /**
     * GUI component containing a logical ChessBoard object.
     */
    private class ChessBoardPanel extends JPanel
    {
        /**
         * Size of each cell or square on the chess board; nothing magic about
         * this number other than it results in a window slightly smaller than
         * 1024x768, which should fit nicely on most laptop screens.
         */
        private static final int CELL_SIZE = 64;

        /** A reference to the main GUI to update turn indicators. */
        private ShogiGUI shogiGui;

        /**
         * Indicates which player's turn it is. Must be equal to ChessBoard.UP
         * or ChessBoard.DOWN (-1). Accessor/mutator methods provided, but
         * variable is kept private to prevent invalid values.
         */
        private int turn;

        /** Logical chess board being viewed in this chess board panel. */
        private ChessBoard board;

        /**
         * Button where the user has currently placed the mouse; setting
         * this causes the red circles indicating valid moves/drops for
         * the chess piece to display.
         */
        private ChessPieceButton activeButton;

        /** List of pieces captured by the player moving down. */
        private ArrayList<ChessPieceButton> downCaptured;

        /** List of pieces captured by the player moving up. */
        private ArrayList<ChessPieceButton> upCaptured;

        /**
         * Creates the panel and a new ChessBoard with initial piece placement.
         *
         * @param shogiGui the main GUI frame containing this panel.
         */
        private ChessBoardPanel( ShogiGUI shogiGui )
        {
            // Initial setup of GUI components.
            this.shogiGui = shogiGui;
            this.activeButton = null;
            this.downCaptured = new ArrayList<>();
            this.upCaptured = new ArrayList<>();
            this.turn = ChessBoard.UP;

            // Width has room for five extra columns to leave a blank column
            // and then four columns for the captures pieces. Both width and
            // height have an extra pixel so the edges look nice.
            int width = CELL_SIZE * (ChessBoard.SIZE + 5) + 1;
            int height = CELL_SIZE * ChessBoard.SIZE + 1;
            this.setPreferredSize( new Dimension( width, height ) );
            this.setBackground( Color.BLUE );
            this.setFont( new Font( "Serif", Font.BOLD, CELL_SIZE / 2 ) );

            // Create the logical chess board and initialize it with starting pieces.
            this.board = new ChessBoard();
            this.board.init();

            // Find each piece on the logical chess board and create a button for it.
            ChessPiece p;
            for( int r = 1; r <= ChessBoard.SIZE; r++ )
            {
                for( int c = 1; c <= ChessBoard.SIZE; c++ )
                {
                    p = this.board.getPieceAt( r, c );
                    if( p != null )
                    {
                        this.add( new ChessPieceButton( p, this ) );
                    }
                }
            }
        }

        /**
         * Accessor method for the current turn.
         *
         * @return this.turn; will be either ChessBoard.DOWN or ChessBoard.UP.
         */
        private int getTurn()
        {
            return this.turn;
        }

        /**
         * Mutator method to change player turns.
         */
        private void changeTurns()
        {
            // Alternates this.turn between +1 and -1.
            this.turn = this.turn * -1;
            this.shogiGui.updateTurnIndicator();
        }

        /**
         * Arranges the captured pieces (buttons) in the captured area.
         *
         * @param direction indicates which list of captured pieces to arrange.
         */
        private void arrangeCaptured( int direction )
        {
            int y, dy, x = CELL_SIZE * (ChessBoard.SIZE + 1);
            ArrayList<ChessPieceButton> buttons;
            if( direction == ChessBoard.DOWN )
            {
                buttons = this.downCaptured;
                y = 0;
                // If there are more than 4x4 captured pieces, they will overlap a bit.
                // This will not be pretty, but it is not likely to happen often.
                dy = Math.min( CELL_SIZE, CELL_SIZE * 4 / ((buttons.size() - 1) / 4 + 1) );
            }
            else
            {
                buttons = this.upCaptured;
                y = CELL_SIZE * (ChessBoard.SIZE - 1);
                // See comment above regarding dy calculation.
                dy = -Math.min( CELL_SIZE, CELL_SIZE * 4 / ((buttons.size() - 1) / 4 + 1) );
            }
            int c = 0;
            for( ChessPieceButton b : buttons )
            {
                b.setLocation( x + c * CELL_SIZE + 2, y + 2 );
                c++;
                if( x + (c + 1) * CELL_SIZE >= this.getWidth() )
                {
                    c = 0;
                    y += dy;
                }
            }
        }

        /**
         * Moves a chess piece (button) to the top of the components on the
         * panel so when it is moved it will always be visible.
         *
         * @param button the ChessPieceButton to move to the top of the component list.
         */
        private void moveToTop( ChessPieceButton button )
        {
            remove( button );
            add( button, 0 );
        }

        /**
         * Paints the ChessBoardPanel.
         *
         * @param g the graphics object to use for painting.
         */
        @Override
        public void paintComponent( Graphics g )
        {
            super.paintComponent( g );

            // Draw the grid and the chess pieces.
            int x, y = 0;
            for( int row = ChessBoard.SIZE; row > 0; row--, y += CELL_SIZE )
            {
                x = 0;
                for( int col = 1; col <= ChessBoard.SIZE; col++, x += CELL_SIZE )
                {
                    g.setColor( Color.BLACK );
                    g.drawRect( x, y, CELL_SIZE, CELL_SIZE );

                    // Draw markers for promotion zones.
                    if( (((row == 3) || (row == 6))) && (((col == 4) || (col == 7))) )
                    {
                        g.setColor( Color.BLACK );
                        g.fillOval( x - 4, y - 4, 8, 8 );
                    }

                    // Note: This will draw the red circles on the board, but the
                    // ChessPieceButtons are drawn on top, so those must in turn
                    // re-draw the red circle on top of itself if necessary.
                    if( this.activeButton != null )
                    {
                        if( !this.activeButton.isCaptured() && this.activeButton.validMove( row, col ) ||
                                this.activeButton.isCaptured() && this.activeButton.validDrop( row, col ) )
                        {
                            g.setColor( Color.RED );
                            g.fillOval( x + CELL_SIZE / 2 - CELL_SIZE / 6, y + CELL_SIZE / 2 - CELL_SIZE / 6,
                                    CELL_SIZE / 3, CELL_SIZE / 3 );
                        }
                    }
                }
            }

            // Draw the 4x4 captured area borders on the right of the board.
            x = CELL_SIZE * (ChessBoard.SIZE + 1);
            g.setColor( Color.BLACK );
            g.drawRect( x, 0, CELL_SIZE * 4, CELL_SIZE * 4 );
            g.drawRect( x, CELL_SIZE * (ChessBoard.SIZE / 2 + 1), CELL_SIZE * 4, CELL_SIZE * 4 );
            g.drawString( "Captured Pieces", x, this.getHeight() / 2 + CELL_SIZE / 4 );
        }
    }

    /**
     * GUI component containing a logical ChessPiece object.
     */
    private class ChessPieceButton extends JButton implements MouseListener, MouseMotionListener
    {
        /** Prefix added to ChessPiece class names when loading icon image. */
        private final String[] DIRECTIONS = { "Down", "", "Up" };

        /** Logical chess piece contained in this button. */
        private ChessPiece chessPiece;
        /** Indicates if this piece has been captured. */
        private boolean captured;
        /** Buttons need to know what panel they are on. */
        private ChessBoardPanel boardPanel;
        /** Used to make buttons drag nicely. */
        private Point mouseDragPoint;

        /**
         * Constructs a new button with the given logical chess piece.
         *
         * @param piece      the logical chess piece represented by this button.
         * @param boardPanel the ChessBoardPanel that contains this button.
         */
        private ChessPieceButton( ChessPiece piece, ChessBoardPanel boardPanel )
        {
            // Save the logical chess piece on the button and the panel this button is on.
            this.chessPiece = piece;
            this.boardPanel = boardPanel;
            this.captured = false;

            // Use the same background color as the panel, no board, and not keyboard focusable.
            this.setBackground( this.boardPanel.getBackground() );
            this.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
            this.setFocusable( false );

            // Offset by 2 to avoid covering the cell borders.
            this.setLocation( (piece.getCurrentCol() - 1) * ChessBoardPanel.CELL_SIZE + 2,
                    (ChessBoard.SIZE - piece.getCurrentRow()) * ChessBoardPanel.CELL_SIZE + 2 );
            this.setSize( ChessBoardPanel.CELL_SIZE - 4, ChessBoardPanel.CELL_SIZE - 4 );
            this.setIcon();

            // Each button will listen for its own events.
            addMouseListener( this );
            addMouseMotionListener( this );
        }

        /**
         * Uses the class name of the logical chess piece and the chess
         * piece's direction to determine the image file name.
         */
        private void setIcon()
        {
            // Use the name of the chess piece's direction and class name to build a file name.
            String pieceName = DIRECTIONS[ this.chessPiece.getDirection() + 1 ] + "_" +
                    this.chessPiece.getClass().getSimpleName();
            String fileName = "images/" + pieceName + ".png";

            // Make sure the file exists and set the image icon.
            File f = new File( fileName );
            if( f.exists() )
            {
                this.setIcon( new ImageIcon( fileName ) );
            }
            else
            {
                System.err.println( "ERROR: Image file not found: " + f.getAbsolutePath() );
                this.setForeground( Color.YELLOW );
                this.setFont( new Font( "Serif", Font.BOLD, this.getHeight() * 4 / 5 ) );
                this.setText( "?" );
            }
        }

        /**
         * Determines if the given row,col is a valid move for the logical chess
         * piece represented by this button.
         *
         * @param row row to which the chess piece is attempting to move.
         * @param col column to which the chess piece is attempting to move.
         *
         * @return true if row,col represents a valid move for this chess piece; false otherwise.
         */
        private boolean validMove( int row, int col )
        {
            // Ask the logical chess piece in this button if it is a valid move.
            return this.chessPiece.validMove( this.boardPanel.board, row, col );
        }

        /**
         * Determines if the given row,col is a valid drop for the logical chess
         * piece represented by this button.
         *
         * @param row row to which the chess piece is attempting to drop.
         * @param col column to which the chess piece is attempting to drop.
         *
         * @return true if row,col represents a valid drop for this chess piece; false otherwise.
         */
        private boolean validDrop( int row, int col )
        {
            // Ask the logical chess piece in this button if it is a valid drop.
            return this.chessPiece.validDrop( this.boardPanel.board, row, col );
        }

        /**
         * Determines if this chess piece has been captured.
         *
         * @return true if this piece has been captured; false otherwise.
         */
        private boolean isCaptured()
        {
            return this.captured;
        }

        /**
         * Paints the ChessPieceButton.
         *
         * @param g The graphics object to use for painting.
         */
        @Override
        public void paintComponent( Graphics g )
        {
            super.paintComponent( g );

            // If there is an active piece on the chess board panel, need to draw
            // the red dots to show the valid moves for the active piece.
            // The ChessBoardPanel will draw red dots on empty squares, but
            // the piece button needs to draw a red dot on itself if necessary.
            ChessPieceButton active = this.boardPanel.activeButton;
            if( active != null && !this.isCaptured() )
            {
                if( active.isCaptured() && active.validDrop( this.chessPiece.getCurrentRow(), this.chessPiece.getCurrentCol() ) ||
                        !active.isCaptured() && active.validMove( this.chessPiece.getCurrentRow(), this.chessPiece.getCurrentCol() ) )
                {
                    g.setColor( Color.RED );
                    g.fillOval( ChessBoardPanel.CELL_SIZE / 3, ChessBoardPanel.CELL_SIZE / 3,
                            ChessBoardPanel.CELL_SIZE / 3, ChessBoardPanel.CELL_SIZE / 3 );
                }
            }
        }

        /**
         * Invoked when the mouse enters a ChessPieceButton.
         *
         * @param e MouseEvent object associated with this action.
         */
        public void mouseEntered( MouseEvent e )
        {
            // If it is this button's turn, set this as the active button.
            if( this.chessPiece.getDirection() == this.boardPanel.getTurn() )
            {
                this.boardPanel.activeButton = this;
                this.boardPanel.repaint();
            }
        }

        /**
         * Invoked when the mouse exits a ChessPieceButton.
         *
         * @param e MouseEvent object associated with this action.
         */
        public void mouseExited( MouseEvent e )
        {
            // If this was the active button, it isn't anymore.
            if( this.boardPanel.activeButton == this )
            {
                this.boardPanel.activeButton = null;
                this.boardPanel.repaint();
            }
        }

        /**
         * Invoked when the mouse button is pressed on a ChessPieceButton.
         *
         * @param e MouseEvent object associated with this action.
         */
        public void mousePressed( MouseEvent e )
        {
            // If this is the active button (set by mouseEntered) and it is this button's turn,
            // set the drag point and start dragging this button.
            if( this.boardPanel.activeButton == this && this.chessPiece.getDirection() == this.boardPanel.getTurn() )
            {
                this.mouseDragPoint = e.getPoint();
                this.boardPanel.moveToTop( this );
            }
        }

        /**
         * Invoked when the mouse button is released on  a ChessPieceButton.
         *
         * @param e MouseEvent object associated with this action.
         */
        public void mouseReleased( MouseEvent e )
        {
            // Don't do anything if this button wasn't the active button. This will happen
            // if the user clicks on a button when it is not that button's turn (that is,
            // the mouseReleased method is called, but the mouseEntered and mousePressed
            // methods will not have done anything because it is not this button's turn).
            if( this.boardPanel.activeButton == this )
            {
                // Calculate where the piece was dropped.
                Point buttonLocation = this.getLocation();
                int xCenter = buttonLocation.x + ChessBoardPanel.CELL_SIZE / 2;
                int yCenter = buttonLocation.y + ChessBoardPanel.CELL_SIZE / 2;
                int newRow = ChessBoard.SIZE - yCenter / ChessBoardPanel.CELL_SIZE;
                int newCol = xCenter / ChessBoardPanel.CELL_SIZE + 1;

                // If it was dropped off the board, in the same place it started, or on an invalid spot, put it back.
                if( xCenter <= 0 || xCenter >= this.boardPanel.getWidth() ||
                        yCenter <= 0 || yCenter >= this.boardPanel.getHeight() ||
                        newRow < 1 || newRow > ChessBoard.SIZE || newCol < 1 || newCol > ChessBoard.SIZE ||
                        (newRow == this.chessPiece.getCurrentRow() && newCol == this.chessPiece.getCurrentCol()) ||
                        (!this.captured && !this.validMove( newRow, newCol )) ||
                        (this.captured && !this.validDrop( newRow, newCol )) )
                {
                    if( this.captured )
                    {
                        this.boardPanel.arrangeCaptured( this.chessPiece.getDirection() );
                    }
                    else
                    {
                        buttonLocation.x = ((this.chessPiece.getCurrentCol() - 1) * ChessBoardPanel.CELL_SIZE + 2);
                        buttonLocation.y = ((ChessBoard.SIZE - this.chessPiece.getCurrentRow()) * ChessBoardPanel.CELL_SIZE + 2);
                        this.setLocation( buttonLocation );
                    }
                }
                else // Deal with a valid move/drop.
                {
                    // If this move results in a capture, deal with the captured piece.
                    if( this.boardPanel.board.getPieceAt( newRow, newCol ) != null )
                    {
                        // Find the button containing the captured piece. (Remove and then
                        // re-add this button so it is not found at these coordinates instead.)
                        // Calculate the x,y coordinate of the center of the button based
                        // on newRow,newCol instead of using xCenter,yCenter just in case
                        // that point is exactly on a grid line and misses a button.
                        this.boardPanel.remove( this );
                        ChessPieceButton b = (ChessPieceButton) this.boardPanel.getComponentAt(
                                newCol * ChessBoardPanel.CELL_SIZE - ChessBoardPanel.CELL_SIZE / 2,
                                (ChessBoard.SIZE - newRow) * ChessBoardPanel.CELL_SIZE + ChessBoardPanel.CELL_SIZE / 2 );
                        this.boardPanel.add( this );

                        // The piece is now captured.
                        b.captured = true;
                        // Remove the piece from the chess board.
                        this.boardPanel.board.setPieceAt( newRow, newCol, null );

                        // Captured pieces are demoted, if necessary.
                        if( b.chessPiece instanceof Demotable )
                        {
                            b.chessPiece = ((Demotable) b.chessPiece).demote();
                        }

                        // Captured pieces change direction to the same as the capturing piece.
                        b.chessPiece.setDirection( this.chessPiece.getDirection() );
                        b.setIcon();

                        // Add the captured piece to the appropriate captured list.
                        if( this.boardPanel.getTurn() == ChessBoard.UP )
                        {
                            this.boardPanel.upCaptured.add( b );
                            this.boardPanel.arrangeCaptured( ChessBoard.UP );
                        }
                        else
                        {
                            this.boardPanel.downCaptured.add( b );
                            this.boardPanel.arrangeCaptured( ChessBoard.DOWN );
                        }
                    }

                    // If the piece was captured, it is now free!
                    if( this.captured )
                    {
                        this.captured = false;
                        if( this.chessPiece.getDirection() == ChessBoard.UP )
                        {
                            this.boardPanel.upCaptured.remove( this );
                            this.boardPanel.arrangeCaptured( ChessBoard.UP );
                        }
                        else
                        {
                            this.boardPanel.downCaptured.remove( this );
                            this.boardPanel.arrangeCaptured( ChessBoard.DOWN );
                        }
                    }
                    else
                    {
                        // Promote chess piece if necessary. (Captured pieces don't promote.)
                        if( this.chessPiece instanceof Promotable )
                        {
                            if( (this.chessPiece.getDirection() == ChessBoard.UP && newRow >= 7) ||
                                    (this.chessPiece.getDirection() == ChessBoard.DOWN && newRow <= 3) )
                            {
                                this.chessPiece = ((Promotable) this.chessPiece).promote();
                                this.setIcon();
                                this.boardPanel.board.setPieceAt( chessPiece.getCurrentRow(), chessPiece.getCurrentCol(), chessPiece );
                            }
                        }

                        // Remove the piece from its old location on the board.
                        this.boardPanel.board.setPieceAt( this.chessPiece.getCurrentRow(), this.chessPiece.getCurrentCol(), null );
                    }

                    // Finally, set the location of the chess piece and move the button to the new square.
                    this.chessPiece.setLocation( newRow, newCol );
                    this.boardPanel.board.setPieceAt( newRow, newCol, this.chessPiece );
                    buttonLocation.x = ((this.chessPiece.getCurrentCol() - 1) * ChessBoardPanel.CELL_SIZE + 2);
                    buttonLocation.y = ((ChessBoard.SIZE - this.chessPiece.getCurrentRow()) * ChessBoardPanel.CELL_SIZE + 2);
                    this.setLocation( buttonLocation );

                    this.boardPanel.changeTurns();
                }
                this.boardPanel.activeButton = null;
                this.boardPanel.repaint();
            }
        }

        /**
         * Invoked when the ChessPieceButton is dragged.
         *
         * @param e MouseEvent object associated with this action.
         */
        public void mouseDragged( MouseEvent e )
        {
            // Can't drag a piece if it's not that piece's turn.
            if( this.boardPanel.activeButton == this && this.chessPiece.getDirection() == this.boardPanel.getTurn() )
            {
                Point buttonLocation = getLocation();
                int dx = e.getX() - this.mouseDragPoint.x;
                int dy = e.getY() - this.mouseDragPoint.y;
                buttonLocation.translate( dx, dy );
                this.setLocation( buttonLocation );
            }
        }

        /**
         * Invoked when the mouse moves on a ChessPieceButton; not used in this
         * application but must be included to fully implement the interface.
         *
         * @param e the MouseEvent object associated with this action.
         */
        public void mouseMoved( MouseEvent e )
        {
        }

        /**
         * Invoked when the mouse is clicked on a ChessPieceButton; not used in this
         * application but must be included to fully implement the interface.
         *
         * @param e the MouseEvent object associated with this action.
         */
        public void mouseClicked( MouseEvent e )
        {
        }
    }
}
