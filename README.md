Code repository for CS 330, Software Design and Development, fall 2017.

Instructions:

1. Fork this repository on BitBicket (be sure to keep the same repository name!)

2. Copy your forked repository's URL from BitBucket (remove "git clone" so the URL starts with "https".)

3. Use IntelliJ on your machine to clone (check out) the repository to your hard drive.
