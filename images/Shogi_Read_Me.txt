Image Files for Shogi Chess Programming Exercise
===== ===== === ===== ===== =========== ========

Notes:

This folder of image files must be in the project folder, not in the src folder.

The Chess Piece classes created for this PEX must have names that exactly match the root of these image file names, Bishop, GoldGeneral, King, Knight, etc.

The "Up_", "Down_", and ".png" will be automatically added to the class names to load the image files, so they must match exactly.

The DragonKing and DragonHorse images are copies of PromotedRook and PromotedBishop so either class name can be used.
