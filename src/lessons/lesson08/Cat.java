package lessons.lesson08;

/**
 * A cat object that has a name, makes noise, moves, and eats.
 * This class is to serve as a super class in an inheritance
 * hierarchy of various types of cats.
 *
 * @author Dr. Randy Bower
 */
public class Cat
{
    /** The name of this cat. */
    private String name;

    /**
     * Construct a new cat with the given name.
     *
     * @param name the name of this cat.
     */
    public Cat( String name )
    {
        this.setName( name );
    }

    /**
     * Mutator (setter) method for this cat's name.
     *
     * @param name the name of this cat.
     * @throws IllegalArgumentException if name is null or empty.
     */
    public void setName( String name )
    {
        if( name == null || name.length() == 0 )
        {
            throw new IllegalArgumentException( "Cat name cannot be null or empty." );
        }
        this.name = name;
    }

    /**
     * Accessor (getter) method for this cat's name.
     *
     * @return the name of this cat.
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * Gets a string indicating the noise this cat makes.
     *
     * @return the noise this cat makes.
     */
    public String getNoise()
    {
        // All cats purr, right?
        return "purr";
    }

    /**
     * Gets a string indicating how this cat moves.
     *
     * @return how this cat moves.
     */
    public String getMove()
    {
        // define: saunter - Walk in a slow, relaxed manner, without hurry or effort.
        return "saunter";
    }

    /**
     * A string representation of this cat with its name, class name, movement, and noise.
     *
     * @return string representation of this cat.
     */
    public String toString()
    {
        // Accessing the class name in this manner is usually
        // a bad idea, but is handy for this simple demo.
        return String.format( "I am %s the %s, watch me %s, hear me %s!",
                this.getName(), this.getClass().getSimpleName(), this.getMove(), this.getNoise() );
    }
}
