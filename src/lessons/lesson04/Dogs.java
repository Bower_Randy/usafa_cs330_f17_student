package lessons.lesson04;

import lessons.lesson02.Dog;

/**
 * Array of Dog objects example from page 62.
 * <p>
 * Example modified to work with our modified Dog class from lesson 2.
 * </p>
 */
public class Dogs
{
    /**
     * Main method to run example.
     *
     * @param args command line arguments; ignored in this application.
     */
    public static void main( String[] args )
    {
        // make a Dog object and access it
        Dog dog1 = new Dog();
        dog1.bark();
        dog1.name = "Bart";

        // now make a Dog array
        Dog[] myDogs = new Dog[ 3 ];

        // and put some dogs in it
        myDogs[ 0 ] = new Dog();
        myDogs[ 1 ] = new Dog();
        myDogs[ 2 ] = dog1;

        // now access the Dogs using the array references
        myDogs[ 0 ].name = "Fred";
        myDogs[ 1 ].name = "Marge";

        // Hmmmm... what is myDogs[2] name?
        System.out.println( "last dog’s name is " + myDogs[ 2 ].name );

        // now loop through the array and tell all dogs to bark
        int x = 0;
        while( x < myDogs.length )
        {
            myDogs[ x ].bark();
            x = x + 1;
        }
    }
}
