package pexes.pex1;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Snails crawl straight forward and backward in the bottom quarter
 * of the aquarium.
 * <p>
 * Snails do not react after meeting another creatures. (Life in a shell!)
 * </p>
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Snail extends Creature
{
    // **** NOTE: For this programming exercise, all class attributes
    // **** for all creatures must be declared as private.

    /** Image to be drawn on the aquarium for this snail. */
    private BufferedImage image;
    /** Current x position of this snail. */
    private double x;
    /** Current y position of this snail. */
    private double y;
    /** Current z position of this snail. */
    private double z;
    /**
     * The scale of a snail. This is determined by its distance from the
     * front of the aquarium; see note in Aquarium about Aquarium.DEPTH.
     */
    private double scale;
    /**
     * Speed this creature is moving. Positive values indicate left-to-right,
     * top-to-bottom, or back-to-front; negative values indicate the opposite.
     * Absolute value of speed must be between 0.5 and 2.0.
     */
    private double speed;

    /**
     * Constructor creates a new Snail with properties set to random values
     * within the bounds of the aquarium and subject to the restrictions of the
     * Snail behavior.
     */
    public Snail()
    {
        // Use the class name to load the image file.
        this.image = Aquarium.loadImage( this.getClass().getSimpleName() );

        // The x, y, and z coordinates are random within the aquarium
        // with (0,0,0) being the top, left, rear corner.
        this.x = Math.random() * Aquarium.WIDTH;
        this.y = Math.random() * Aquarium.HEIGHT;
        this.z = Math.random() * Aquarium.DEPTH;

        // Scale depends on the z coordinate. See comment above.
        this.scale = 0.5 + this.z / Aquarium.DEPTH / 2.0;

        // Speed is a random value between 0.5 and 2.0.
        this.speed = ( 0.5 + Math.random() * 1.5 ) * Math.random() < 0.5 ? 1 : -1;

        // With 0.5 probability, invert speed to move in the opposite direction.
        if( Math.random() < 0.5 )
        {
            this.speed *= -1;
        }

        // The z position of a crawler determines its y position, so call
        // the setZ method with the current z position (which is admittedly
        // redundant) so the method can also set the y position properly.
        this.setZ( this.getZ() );
    }

    /**
     * Determine if this snail has met the given angelfish.
     *
     * @param that The angelfish which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Angelfish that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Determine if this snail has met the given clownfish.
     *
     * @param that The clownfish which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Clownfish that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Determine if this snail has met the given crab.
     *
     * @param that The crab which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Crab that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Determine if this snail has met the given lobster.
     *
     * @param that The lobster which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Lobster that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Determine if this snail has met the given seahorse.
     *
     * @param that The seahorse which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Seahorse that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Determine if this snail has met the given snail.
     *
     * @param that The snail which has possibly met this snail.
     *
     * @return true if the two have met; false otherwise.
     */
    public boolean met( Snail that )
    {
        double dx = Math.abs( this.getX() - that.getX() );
        if( this.getX() <= that.getX() && dx <= this.getWidth() ||
                that.getX() <= this.getX() && dx <= that.getWidth() )
        {
            double dy = Math.abs( this.getY() - that.getY() );
            if( this.getY() <= that.getY() && dy <= this.getHeight() ||
                    that.getY() <= this.getY() && dy <= that.getHeight() )
            {
                double dz = Math.abs( this.getZ() - that.getZ() );
                if( this.getZ() <= that.getZ() && dz <= this.getDepth() ||
                        that.getZ() <= this.getZ() && dz <= that.getDepth() )
                {
                    // Only meet if going in opposite directions.
                    return this.getSpeed() * that.getSpeed() < 0;
                }
            }
        }
        return false;
    }

    /**
     * Snails move forward and backward in the aquarium, turning
     * around when they reach the front or back of the aquarium and
     * also moving left or right when at the front of the aquarium.
     */
    public void move()
    {
        // Move forward or backward, based on speed.
        this.setZ( this.getZ() + this.getSpeed() );

        // If necessary, turn around.
        if( this.getZ() <= 0 || this.getZ() >= Aquarium.DEPTH )
        {
            this.setSpeed( this.getSpeed() * -1 );
        }

        // If at the front of the aquarium, move left or right.
        if( this.getZ() >= Aquarium.DEPTH )
        {
            if( Math.random() < 0.5 )
            {
                this.setX( Math.min( this.getX() + this.getWidth(), Aquarium.WIDTH ) );
            }
            else
            {
                this.setX( Math.max( this.getX() - this.getWidth(), 0 ) );
            }
        }
    }

    /**
     * Paints the creature's image with the given graphics object.
     *
     * @param g The Graphics object to use to paint this creature.
     */
    public void paint( Graphics g )
    {
        // The speed determines if it is facing left or right.
        if( this.speed > 0 )
        {
            // Positive speed indicates facing right.
            g.drawImage( this.image, (int) x, (int) y,
                    (int) (x + this.image.getWidth() * this.scale),
                    (int) (y + this.image.getHeight() * this.scale),
                    0, 0, this.image.getWidth(), this.image.getHeight(), null );
        }
        else
        {
            // Negative speed indicates facing left. Notice the second and
            // fourth parameters, which are the x-coordinates of the image
            // being drawn, are inverted from the right-facing image above.
            g.drawImage( this.image, (int) (x + this.image.getWidth() * this.scale), (int) y,
                    (int) x, (int) (y + this.image.getHeight() * this.scale),
                    0, 0, this.image.getWidth(), this.image.getHeight(), null );
        }
    }

    /**
     * Sets the x position of this creature, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * creature. Creatures are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the creature as well as the aquarium's dimensions.
     *
     * @param x New x position for this creature.
     */
    public void setX( double x )
    {
        // Can go off the right and left edges of the aquarium.
        this.x = Math.max( Math.min( x, Aquarium.WIDTH ), -this.getWidth() );
    }

    /**
     * A crab's y position is determined by its z position and is set by setZ.
     *
     * @param y New y position for this creature; <strong>ignored</strong>.
     */
    public void setY( double y )
    {
        // Do nothing; this value is determined by the z position.
    }

    /**
     * Sets the z position of this creature, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * creature. Creatures are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the creature as well as the aquarium's dimensions.
     * <p>
     * This method calculates the y position after setting the z position.
     * </p>
     * <p>
     * <strong>Note</strong>: The depth of the aquarium is the distance
     * from the back to the front of the aquarium, not the water depth.
     * </p>
     *
     * @param z New z position for this creature.
     */
    public void setZ( double z )
    {
        // Cannot go off the back or the front of the aquarium.
        this.z = Math.max( Math.min( z, Aquarium.DEPTH ), 0 );

        // A creature's scale is determined by its z position. That is, the
        // perceived size depends on how far it is from the front the aquarium.
        this.scale = 0.5 + this.z / Aquarium.DEPTH / 2.0;

        // The y position of a crawler must be in the bottom quarter of the aquarium.
        // Exactly where in the bottom quarter is relative to the z position.
        this.y = Aquarium.HEIGHT * 0.75 + Aquarium.HEIGHT * 0.25 *
                this.getZ() / Aquarium.DEPTH;
    }

    /**
     * Sets the speed of this creature, which must be between 0.5 and 2.0,
     * positive or negative.
     *
     * @param speed New speed position for this creature.
     *
     * @throws IllegalArgumentException If speed is out of range.
     */
    public void setSpeed( double speed )
    {
        if( Math.abs( speed ) < 0.5 || Math.abs( speed ) > 2.0 )
        {
            throw new IllegalArgumentException( "speed is out of range: " + speed );
        }
        this.speed = speed;
    }

    /**
     * Convenience method to set the location of this creature.
     *
     * @param x     New x position for this creature.
     * @param y     New y position for this creature.
     * @param z     New z position for this creature.
     */
    public void setXYZ( double x, double y, double z )
    {
        // Must set z first as this also sets scale which determines
        // width and height which are used in both setY and setX.
        this.setZ( z );
        this.setY( y );
        this.setX( x );
    }

    /**
     * Accessor method for the x position of this creature.
     *
     * @return The current x position of this creature.
     */
    public double getX()
    {
        return this.x;
    }

    /**
     * Accessor method for the y position of this creature.
     *
     * @return The current y position of this creature.
     */
    public double getY()
    {
        return this.y;
    }

    /**
     * Accessor method for the z position of this creature.
     *
     * @return The current z position of this creature.
     */
    public double getZ()
    {
        return this.z;
    }

    /**
     * Accessor method for the speed of this creature.
     *
     * @return The current speed of this creature.
     */
    public double getSpeed()
    {
        return this.speed;
    }

    /**
     * Accessor method for the scale of this creature.
     *
     * @return The current scale of this creature.
     */
    public double getScale()
    {
        return this.scale;
    }

    /**
     * Accessor method for the width of this creature. It is determined by
     * the current scale of the creature and the width of the creature's image.
     *
     * @return The current width of this creature.
     */
    public double getWidth()
    {
        return this.image.getWidth() * this.scale;
    }

    /**
     * Accessor method for the height of this creature. It is determined by
     * the current scale of the creature and the height of the creature's image.
     *
     * @return The current height of this creature.
     */
    public double getHeight()
    {
        return this.image.getHeight() * this.scale;
    }

    /**
     * Accessor method for the depth of this creature. It is determined by
     * the current scale of the creature and is somewhat arbitrarily set
     * to one-half of the height of the creature.
     * <p>
     * <strong>Note</strong>: The depth of a creature is its "thickness"
     * or the amount of space it occupies front to back in the aquarium.
     * </p>
     *
     * @return The current depth of this creature.
     */
    public double getDepth()
    {
        return this.getHeight() / 2;
    }

    /**
     * Creates and returns a string representation of this snail that
     * includes the position, size, scale, and speed of the snail.
     * Only used for debugging, so it's not very fancy.
     *
     * @return A string representation of this creature.
     */
    @Override
    public String toString()
    {
        return String.format( "%s (%.1f, %.1f, %.1f), (%.1f, %.1f, %.1f), %.1f, %.1f",
                this.getClass().getSimpleName(), this.getX(), this.getY(), this.getZ(),
                this.getWidth(), this.getHeight(), this.getDepth(), this.getScale(), this.getSpeed() );
    }
}
