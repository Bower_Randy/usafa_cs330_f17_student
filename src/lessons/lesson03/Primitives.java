package lessons.lesson03;

/**
 * Static methods demonstrating primitive data types.
 */
public class Primitives
{
    /**
     * Main method to launch each experiment.
     *
     * @param args command line arguments; ignored in this application.
     */
    public static void main( String[] args )
    {
        boundaries();
        casting();
        overflow();
        rounding();
        tolerance();
    }

    /**
     * Show boundary values for each primitive type.
     * <p><pre>
     * Be Careful! Bears Shouldn't Ingest Large Furry Dogs
     * o  h        y     h         n      o     l     o
     * o  a        t     o         t      n     o     u
     * l  r        e     r                g     a     b
     * e                 t                      t     l
     * a                                              e
     * n
     * </pre></p>
     */
    private static void boundaries()
    {
        System.out.println( "boundaries():" );
        System.out.println( "  Minimum character value is " + (int) Character.MIN_VALUE );
        System.out.println( "  Maximum character value is " + (int) Character.MAX_VALUE );
        System.out.println( "  Minimum byte value is " + Byte.MIN_VALUE );
        System.out.println( "  Maximum byte value is " + Byte.MAX_VALUE );
        System.out.println( "  Minimum short value is " + Short.MIN_VALUE );
        System.out.println( "  Maximum short value is " + Short.MAX_VALUE );
        System.out.println( "  Minimum int value is " + Integer.MIN_VALUE );
        System.out.println( "  Maximum int value is " + Integer.MAX_VALUE );
        System.out.println( "  Minimum long value is " + Long.MIN_VALUE );
        System.out.println( "  Maximum long value is " + Long.MAX_VALUE );
        System.out.println( "  Minimum float value is " + Float.MIN_VALUE );
        System.out.println( "  Maximum float value is " + Float.MAX_VALUE );
        System.out.println( "  Minimum double value is " + Double.MIN_VALUE );
        System.out.println( "  Maximum double value is " + Double.MAX_VALUE );
    }

    /**
     * Demonstrate type casting.
     */
    private static void casting()
    {
        System.out.println( "casting()" );
        int a = 2, b = 3, c;
        float f, g, h;

        c = a / b;
        f = a / b;
        g = (float)(a / b);
        h = (float) a / b;

        System.out.println("  c = " + c );
        System.out.println("  f = " + f );
        System.out.println("  g = " + g );
        System.out.println("  h = " + h );
    }

    /**
     * Demonstrate integer overflow.
     */
    private static void overflow()
    {
        System.out.println( "overflow()" );
        int i = Integer.MAX_VALUE;
        System.out.println( "  i = " + i );
        i = i + 1;
        System.out.println( "  i = " + i );
    }

    /**
     * Demonstrate rounding error with float/double values.
     */
    private static void rounding()
    {
        System.out.println( "rounding()" );
        double d = 0.0;
        for( int i = 0; i < 10; i++ )
        {
            d = d + 0.1;  // Add one-tenth ten times.
        }
        // d should be equal to 1.0, right?
        System.out.println( "  d = " + d );
    }

    /** Acceptable tolerance for considering double values equal. */
    private static final double TOLERANCE = 0.0000000001;

    /**
     * Demonstrate how to compare float/double values.
     */
    private static void tolerance()
    {
        System.out.println( "tolerance()" );
        double a = 1.0 / 3.0;  // one-third
        double b = a * 2;  // two-thirds
        double c = 1.0 - a;  // also two-thirds
        System.out.println( "  b == c is " + ( b == c ) );  // false!
        System.out.println( "  Math.abs( b - c ) < TOLERANCE is " + ( Math.abs( b - c ) < TOLERANCE ) );
    }
}
