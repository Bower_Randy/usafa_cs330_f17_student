package lessons.lesson06;

/**
 * A fully encapsulated class to represent fractions
 * with numerator and denominator attributes.
 * <p>
 * Full encapsulation ensures the denominator is never zero.
 * </p>
 */
public class Fraction
{
    /** This fraction's numerator. */
    private int numerator;
    /** This fraction's denominator; cannot be zero. */
    private int denominator;

    /**
     * Creates a Fraction object with default value of 0/1.
     */
    public Fraction()
    {
        this( 0, 1 );
    }

    /**
     * Creates a Fraction object with value n/1.
     *
     * @param numerator numerator of this Fraction object.
     */
    public Fraction( int numerator )
    {
        this( numerator, 1 );
    }

    /**
     * Creates a Fraction object with the same value as the given fraction.
     * (Called a "copy" constructor and is provided for convenience.)
     *
     * @param fraction Fraction object to copy.
     */
    public Fraction( Fraction fraction )
    {
        this( fraction.getNumerator(), fraction.getDenominator() );
    }

    /**
     * Creates a Fraction object by parsing a string in the form "n/d"
     * where n and d are integers.
     *
     * @param fraction string in the form "n/d".
     */
    public Fraction( String fraction )
    {
        this( Integer.parseInt( fraction.split( "/" )[ 0 ] ),
                Integer.parseInt( fraction.split( "/" )[ 1 ] ) );
    }

    /**
     * Creates a Fraction object with the given values for
     * numerator and denominator.
     *
     * @param numerator numerator of this Fraction object.
     * @param denominator denominator of this Fraction object.
     */
    public Fraction( int numerator, int denominator )
    {
        this.setNumerator( numerator );
        this.setDenominator( denominator );
    }

    /**
     * Mutator method to set the numerator of this Fraction object.
     *
     * @param numerator value to set as the numerator of this Fraction object.
     */
    public void setNumerator( int numerator )
    {
        this.numerator = numerator;
    }

    /**
     * Mutator method to set the denominator of this Fraction object.
     *
     * @param denominator value to set as the denominator of this Fraction object; <strong>cannot be zero!</strong>
     */
    public void setDenominator( int denominator )
    {
        if( denominator == 0 )
        {
            throw new IllegalArgumentException( "Fraction denominator cannot be zero." );
        }
        this.denominator = denominator;
    }

    /**
     * Accessor method for the numerator of this fraction object.
     *
     * @return The value of the numerator of this fraction object.
     */
    public int getNumerator()
    {
        return this.numerator;
    }

    /**
     * Accessor method for the denominator of this fraction object.
     *
     * @return The value of the denominator of this fraction object.
     */
    public int getDenominator()
    {
        return this.denominator;
    }

    /**
     * Creates and returns a string representation of this fraction object.
     *
     * @return string representation of this fraction object.
     */
    @Override
    public String toString()
    {
        return this.getNumerator() + "/" + this.getDenominator();
    }

    /**
     * Main method to do some testing with this class.
     *
     * @param args command line arguments; ignored in this application.
     */
    public static void main( String[] args )
    {
        // Create and print various fraction objects to test each
        // constructor which also tests the mutator and accessor
        // methods as well as the toString method.
        System.out.println( new Fraction() );
        System.out.println( new Fraction( 3 ) );
        System.out.println( new Fraction( 5, 8 ) );
        System.out.println( new Fraction( "9/16" ) );
        System.out.println( new Fraction( new Fraction( "12/345" ) ) );
    }
}
