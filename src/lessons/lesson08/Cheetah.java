package lessons.lesson08;

/**
 * Cheetah class as part of the Cat hierarchy.
 *
 * @author Dr. Randy Bower
 */
public class Cheetah extends Cat
{
    /**
     * Construct a new cheetah with the given name.
     *
     * @param name the name of this cheetah.
     */
    public Cheetah( String name )
    {
        super( name );
    }
}
