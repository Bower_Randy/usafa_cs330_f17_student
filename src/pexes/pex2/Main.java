package pexes.pex2;

/**
 * Main class with a static main method to launch the application.
 * <p>
 * <strong>HW Documentation</strong>: _YOUR_DETAILED_DOCUMENTATION_STATEMENT_HERE_.
 * <p>
 * <strong>PEX Documentation</strong>: _YOUR_DETAILED_DOCUMENTATION_STATEMENT_HERE_.
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Main
{
    /**
     * The main method launches the application.
     *
     * @param args Command line arguments.
     */
    public static void main( String[] args )
    {
        ShogiGUI.launch();
    }
}
