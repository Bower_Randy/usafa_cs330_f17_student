package lessons.lesson08;

/**
 * Cartoon cat class as part of the Cat hierarchy.
 *
 * @author Dr. Randy Bower
 */
public class Cartoon extends Cat
{
    /**
     * Construct a new cartoon cat with the given name.
     *
     * @param name the name of this cartoon cat.
     */
    public Cartoon( String name )
    {
        super( name );
    }
}
