package pexes.pex2;

/**
 * A Pawn can move one space straight forward; a Pawn promotes to a Gold General.
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Pawn extends ChessPiece implements Promotable
{
    /**
     * All chess piece classes must have exactly one constructor
     * that passes along all parameters to super.
     *
     * @param direction direction this chess piece is moving.
     * @param row       row of this chess piece.
     * @param col       column of this chess piece.
     */
    public Pawn( int direction, int row, int col )
    {
        super( direction, row, col );
    }

    /**
     * A Pawn can move one space straight forward.
     *
     * @param board the ChessBoard on which this Pawn is attempting to move.
     * @param row   row to which the Pawn is attempting to move.
     * @param col   column to which the Pawn is attempting to move.
     *
     * @return true if (row,col) represents a valid move for this Pawn; false otherwise.
     */
    @Override
    public boolean validMove( ChessBoard board, int row, int col )
    {
        return super.validMove( board, row, col );  // TODO: Add to this line of code, as necessary.
    }

    /**
     * A Pawn can not be dropped in the last row making it impossible to move
     * or in a column with another non-prompted Pawn on the same team.
     *
     * @param board the ChessBoard on which this Pawn is attempting to drop.
     * @param row   row to which the Pawn is attempting to drop.
     * @param col   column to which the Pawn is attempting to drop.
     *
     * @return true if (row,col) represents a valid drop for this Pawn; false otherwise.
     */
    @Override
    public boolean validDrop( ChessBoard board, int row, int col )
    {
        return super.validDrop( board, row, col );  // TODO: Add to this line of code, as necessary.
    }

    /**
     * Creates and returns the promoted version of this chess piece.
     *
     * @return promoted version of this chess piece.
     */
    public ChessPiece promote()
    {
        return this;  // TODO: Replace this line with your own code.
    }
}
