package pexes.pex1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for the creatures in the aquarium.
 *
 * @author Dr. Randy Bower
 */
public class UnitTests
{
    // Number of times to repeat tests that involve randomness.
    private static final int N = 42;  // Why 42? Why not?!

    // Tolerance for considering double values equal.
    private static final double TOLERANCE = 0.0001;

    /**
     * Test the Angelfish constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void angelfishConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Angelfish nemo = new Angelfish();
            // Angelfish must be in the upper four-fifths of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= 0 && nemo.getY() <= Aquarium.HEIGHT * 4 / 5 );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the Clownfish constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void clownfishConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Clownfish nemo = new Clownfish();
            // Clownfish must be in the upper four-fifths of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= 0 && nemo.getY() <= Aquarium.HEIGHT * 4 / 5 );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the Seahorse constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void seahorseConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Seahorse nemo = new Seahorse();
            // Seahorse must be in the upper four-fifths of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= 0 && nemo.getY() <= Aquarium.HEIGHT * 4 / 5 );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the Crab constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void crabConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Crab nemo = new Crab();
            // Crab must be in the lower quarter of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= Aquarium.HEIGHT * 3 / 4 && nemo.getY() <= Aquarium.HEIGHT );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the Lobster constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void lobsterConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Lobster nemo = new Lobster();
            // Lobster must be in the lower quarter of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= Aquarium.HEIGHT * 3 / 4 && nemo.getY() <= Aquarium.HEIGHT );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the Snail constructor to ensure proper initial placement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void snailConstructor() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            Snail nemo = new Snail();
            // Snail must be in the lower quarter of the aquarium.
            assertTrue( nemo.getX() >= 0 && nemo.getX() <= Aquarium.WIDTH );
            assertTrue( nemo.getY() >= Aquarium.HEIGHT * 3 / 4 && nemo.getY() <= Aquarium.HEIGHT );
            assertTrue( nemo.getZ() >= 0 && nemo.getZ() <= Aquarium.DEPTH );
            // All creatures should be scaled between 0.5 and 1.0.
            assertTrue( nemo.getScale() >= 0.5 && nemo.getScale() <= 1.0 );
            // All creatures are moving at speed between -2.0 and -0.5 or 0.5 and 2.0.
            assertTrue( Math.abs( nemo.getSpeed() ) >= 0.5 && Math.abs( nemo.getSpeed() ) <= 2.0 );
        }
    }

    /**
     * Test the meet method of all creatures that swim in the upper
     * four-fifths of the aquarium.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void meetSwimmers() throws Exception
    {
        // Create two lists of creatures to use in the tests.
        // Use two lists to avoid a creature meeting itself, but
        // still test that two creatures of the same type meet.
        Creature[] a_list = { new Angelfish(), new Clownfish(), new Seahorse() };
        Creature[] b_list = { new Angelfish(), new Clownfish(), new Seahorse() };

        // Nested loops to test each possible pairing of creatures.
        for( Creature a : a_list )
        {
            // Move the first creature to the specific spot in the aquarium.
            a.setXYZ( Aquarium.WIDTH / 2, Aquarium.HEIGHT / 5, Aquarium.DEPTH / 5 );

            for( Creature b : b_list )
            {
                // Move the second creature so it is right on top of the first.
                b.setXYZ( a.getX(), a.getY(), a.getZ() );
                // Creatures should not meet if they are moving the same direction.
                b.setSpeed( a.getSpeed() );
                assertFalse( a.met( b ) || b.met( a ) );
                // Creatures should meet if they are moving opposite directions.
                b.setSpeed( -a.getSpeed() );
                assertTrue( a.met( b ) && b.met( a ) );

                // Move the second creature right, below, and in front of
                // the first, but make sure they are still touching.
                b.setXYZ( a.getX() + a.getWidth() - 1, a.getY(), a.getZ() );
                assertTrue( a.met( b ) && b.met( a ) );
                b.setXYZ( a.getX(), a.getY() + a.getHeight() - 1, a.getZ() );
                assertTrue( a.met( b ) && b.met( a ) );
                b.setXYZ( a.getX(), a.getY(), a.getZ() + a.getDepth() - 1 );
                assertTrue( a.met( b ) && b.met( a ) );

                // Move the second creature right, below, and in front of
                // the first, and make sure they are NOT touching.
                b.setXYZ( a.getX() + a.getWidth() + 1, a.getY(), a.getZ() );
                assertFalse( a.met( b ) || b.met( a ) );
                b.setXYZ( a.getX(), a.getY() + a.getHeight() + 1, a.getZ() );
                assertFalse( a.met( b ) || b.met( a ) );
                b.setXYZ( a.getX(), a.getY(), a.getZ() + a.getDepth() + 1 );
                assertFalse( a.met( b ) || b.met( a ) );
            }
        }
    }

    /**
     * Test the meet method of all creatures that crawl in the lower
     * three-quarters of the aquarium.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void meetCrawlers() throws Exception
    {
        // Create two lists of creatures to use in the tests.
        // Use two lists to avoid a creature meeting itself, but
        // still test that two creatures of the same type meet.
        Creature[] a_list = { new Crab(), new Lobster(), new Snail() };
        Creature[] b_list = { new Crab(), new Lobster(), new Snail() };

        // Nested loops to test each possible pairing of creatures.
        for( Creature a : a_list )
        {
            // Move the first creature to a specific spot in the aquarium.
            a.setXYZ( Aquarium.WIDTH / 2, Aquarium.HEIGHT * 3 / 4, 0 );

            for( Creature b : b_list )
            {
                // Move the second creature so it is right on top of the first.
                b.setXYZ( a.getX(), a.getY(), a.getZ() );
                // Creatures should not meet if they are moving the same direction.
                b.setSpeed( a.getSpeed() );
                assertFalse( a.met( b ) || b.met( a ) );
                // Creatures should meet if they are moving opposite directions.
                b.setSpeed( -a.getSpeed() );
                assertTrue( a.met( b ) && b.met( a ) );

                // Move the second creature right, below, and in front of
                // the first, but make sure they are still touching.
                b.setXYZ( a.getX() + a.getWidth() - 1, a.getY(), a.getZ() );
                assertTrue( a.met( b ) && b.met( a ) );
                b.setXYZ( a.getX(), a.getY() + a.getHeight() - 1, a.getZ() );
                assertTrue( a.met( b ) && b.met( a ) );
                b.setXYZ( a.getX(), a.getY(), a.getZ() + a.getDepth() - 1 );
                assertTrue( a.met( b ) && b.met( a ) );

                // Move the second creature right, below, and in front of
                // the first, and make sure they are NOT touching.
                b.setXYZ( a.getX() + a.getWidth() + 1, a.getY(), a.getZ() );
                assertFalse( a.met( b ) || b.met( a ) );
                b.setXYZ( a.getX(), a.getY(), Aquarium.DEPTH - b.getDepth() );
                assertFalse( a.met( b ) || b.met( a ) );
                b.setXYZ( a.getX(), a.getY(), a.getZ() + a.getDepth() + 1 );
                assertFalse( a.met( b ) || b.met( a ) );
            }
        }
    }

    /**
     * Test the meet method of all creatures in the zone between three-quarters
     * and four-fifths where creatures can both swim and crawl.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void meetAll() throws Exception
    {
        // Create two lists of creatures to use in the tests.
        // Use two lists to avoid a creature meeting itself, but
        // still test that two creatures of the same type meet.
        Creature[] crawler_list = { new Crab(), new Lobster(), new Snail() };
        Creature[] swimmer_list = { new Angelfish(), new Clownfish(), new Seahorse() };

        // Nested loops to test each possible pairing of creatures.
        for( Creature crawler : crawler_list )
        {
            // Move the crawler to the back center of the aquarium, which
            // also puts it as high as it can be so a swimmer can meet it.
            crawler.setXYZ( Aquarium.WIDTH / 2, Aquarium.HEIGHT * 3 / 4, 0 );

            for( Creature swimmer : swimmer_list )
            {
                // Move the swimmer so it is above the crawler, as low as it can go.
                swimmer.setXYZ( crawler.getX(), Aquarium.HEIGHT * 4 / 5, crawler.getZ() );
                // Creatures should not meet if they are moving the same direction.
                crawler.setSpeed( swimmer.getSpeed() );
                assertFalse( swimmer.met( crawler ) || crawler.met( swimmer ) );
                // Creatures should meet if they are moving opposite directions.
                crawler.setSpeed( -swimmer.getSpeed() );
                assertTrue( swimmer.met( crawler ) && crawler.met( swimmer ) );

                // Move the swimmer right and in front of the crawler, but make sure they are still touching.
                swimmer.setXYZ( crawler.getX() + crawler.getWidth() - 1, swimmer.getY(), swimmer.getZ() );
                assertTrue( swimmer.met( crawler ) && crawler.met( swimmer ) );
                swimmer.setXYZ( crawler.getX(), swimmer.getY(), crawler.getZ() + crawler.getDepth() - 1 );
                assertTrue( swimmer.met( crawler ) && crawler.met( swimmer ) );

                // Move the swimmer right and in front of the crawler, and make sure they are NOT touching.
                swimmer.setXYZ( crawler.getX() + crawler.getWidth() + 1, swimmer.getY(), swimmer.getZ() );
                assertFalse( swimmer.met( crawler ) || crawler.met( swimmer ) );
                swimmer.setXYZ( crawler.getX(), swimmer.getY(), crawler.getZ() + crawler.getDepth() + 1 );
                assertFalse( swimmer.met( crawler ) || crawler.met( swimmer ) );
            }
        }
    }

    /**
     * Test the meet method of Bubble to make sure it never meets.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void meetBubbles() throws Exception
    {
        // Create a bubble and a list of creatures.
        Bubble b = new Bubble();
        Creature[] c_list = { new Angelfish(), new Clownfish(), new Seahorse(), new Crab(), new Lobster(), new Snail() };

        for( Creature c : c_list )
        {
            // Move the creature to a specific spot in the aquarium.
            c.setXYZ( Aquarium.WIDTH / 2, Aquarium.HEIGHT * 15.5 / 20, Aquarium.DEPTH / 2 );

            // Move the bubble right on top of the creature.
            b.setXYZ( c.getX(), c.getY(), c.getZ() );

            // Make sure the bubble does not meet the creature, regardless of direction.
            assertFalse( b.met( c ) );
            c.setSpeed( -c.getSpeed() );
            assertFalse( b.met( c ) );
        }
    }

    /**
     * Test Angelfish movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveAngelfish() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Angelfish and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            Angelfish a = new Angelfish();
            a.setX( Math.max( Math.min( a.getX(), Aquarium.WIDTH - a.getWidth() ), 0 ) );
            a.setY( Math.max( Math.min( a.getY(), Aquarium.HEIGHT * 4 / 5 - a.getHeight() * 2 ), a.getHeight() ) );

            // Save the coordinates for testing below, then move it.
            double x = a.getX(), y = a.getY(), z = a.getZ();
            a.move();

            // The Angelfish should have moved horizontally based on its speed.
            assertEquals( "X", Math.abs( x - a.getX() ), Math.abs( a.getSpeed() ), TOLERANCE );
            // The Angelfish should have moved vertically based on its delta.
            assertTrue( "D", Math.abs( a.getDelta() ) > TOLERANCE );
            assertEquals( "Y", Math.abs( y - a.getY() ), Math.abs( a.getDelta() ), TOLERANCE );
            // The Angelfish should not have moved forward or backward.
            assertEquals( "Z", z, a.getZ(), TOLERANCE );
        }
    }

    /**
     * Test Clownfish movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveClownfish() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Clownfish and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            Clownfish c = new Clownfish();
            c.setX( Math.max( Math.min( c.getX(), Aquarium.WIDTH - c.getWidth() ), 0 ) );

            // Save the coordinates for testing below, then move it.
            double x = c.getX(), y = c.getY(), z = c.getZ();
            c.move();

            // The Clownfish should have moved horizontally based on its speed.
            assertEquals( "X", Math.abs( x - c.getX() ), Math.abs( c.getSpeed() ), TOLERANCE );
            // The Clownfish should not have moved vertically.
            assertEquals( "Y", y, c.getY(), TOLERANCE );
            // The Clownfish should not have moved forward or backward.
            assertEquals( "Z", z, c.getZ(), TOLERANCE );
        }
    }

    /**
     * Test Seahorse movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveSeahorse() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Seahorse and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            Seahorse s = new Seahorse();
            s.setX( Math.max( Math.min( s.getX(), Aquarium.WIDTH - s.getWidth() ), 0 ) );

            // Save the coordinates for testing below, then move it.
            double x = s.getX(), y = s.getY(), z = s.getZ();
            s.move();

            // The Seahorse should have moved horizontally based on its speed.
            assertEquals( "X", Math.abs( x - s.getX() ), Math.abs( s.getSpeed() ), TOLERANCE );
            // The Seahorse should have moved vertically based on its delta.
            assertEquals( "D", Math.abs( s.getDelta() ), Math.abs( s.getSpeed() ), TOLERANCE );
            assertEquals( "Y", Math.abs( y - s.getY() ), Math.abs( s.getDelta() ), TOLERANCE );
            // The Seahorse should not have moved forward or backward.
            assertEquals( "Z", z, s.getZ(), TOLERANCE );
        }
    }

    /**
     * Test Crab movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveCrab() throws Exception
    {
        // Calculate the max depth of a Crab to give it room to avoid turns.
        Crab c = new Crab();
        // The z coordinate determines the scale which determines the size.
        c.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = c.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Crab and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            c = new Crab();
            c.setZ( Math.max( Math.min( c.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Save the coordinates for testing below, then move it.
            double x = c.getX(), y = c.getY(), z = c.getZ();
            c.move();

            // The Crab should not have moved horizontally.
            assertEquals( "X", x, c.getX(), TOLERANCE );
            // The Crab should have moved vertically, but this is based on z so just check that it moved some.
            assertTrue( "Y", Math.abs( y - c.getY() ) > TOLERANCE );
            // The Crab should have moved forward or backward based on its speed.
            assertEquals( "Z", Math.abs( z - c.getZ() ), Math.abs( c.getSpeed() ), TOLERANCE );


            // Also need to test that the Crab pauses at times.
            c = new Crab();
            c.setXYZ( c.getX(), Aquarium.HEIGHT * 3 / 4, 0 );  // Back of the aquarium.
            c.setSpeed( 0.5 );  // Moving forward as slow as possible.

            // Count how many times the crab moves and pauses.
            int moves = 0, pauses = 0;

            // Loop until the crab reaches the front and turns around.
            while( c.getSpeed() > 0 )
            {
                // Save the crab's current state and then move the crab.
                String s = c.toString();
                c.move();
                moves++;

                // If the crab's state stayed the same, it didn't move.
                if( s.equals( c.toString() ) )
                {
                    pauses++;
                }
            }

            // Pauses should be at least 1% of the time; likely quite a bit more
            // but due to randomness this will have to be a sufficient test.
            // Test pauses == 0 for the rare occurrence of no random pauses.
            // This is acceptable since the test is repeated numerous times.
            assertTrue( "Pauses", pauses == 0 || pauses > moves * 0.01 );
        }
    }

    /**
     * Test Lobster movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveLobster() throws Exception
    {
        // Calculate the max depth of a Lobster to give it room to avoid turns.
        Lobster lob = new Lobster();
        // The z coordinate determines the scale which determines the size.
        lob.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = lob.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Lobster and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            lob = new Lobster();
            lob.setZ( Math.max( Math.min( lob.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Save the coordinates for testing below, then move it.
            double y = lob.getY(), z = lob.getZ();
            lob.move();

            // The Lobster may or may not have moved horizontally; tested below.
            // The Lobster should have moved vertically, but this is based on z so just check that it moved some.
            assertTrue( "Y", Math.abs( y - lob.getY() ) > TOLERANCE );
            // The Lobster should have moved forward or backward based on its speed.
            assertEquals( "Z", Math.abs( z - lob.getZ() ), Math.abs( lob.getSpeed() ), TOLERANCE );

            // Also need to test that the Lobster moves horizontally at times.
            lob = new Lobster();
            lob.setXYZ( lob.getX(), Aquarium.HEIGHT * 3 / 4, 0 );  // Back of the aquarium.
            lob.setSpeed( 0.5 );  // Moving forward as slow as possible.

            // Count how many times the lobster moves horizontally.
            int moves = 0, horizontal_moves = 0;

            // Loop until the lobster reaches the front and turns around.
            while( lob.getSpeed() > 0 )
            {
                // Save the lobster's current x position and move it.
                double x = lob.getX();
                lob.move();
                moves++;

                // If the lobster's x-coordinate changed, it moved horizontally.
                if( Math.abs( x - lob.getX() ) > TOLERANCE )
                {
                    horizontal_moves++;
                }
            }

            // Horizontal moves should be about half the time; test one-quarter
            // to account for some randomness in how often it moves horizontally.
            assertTrue( "Horizontal moves", horizontal_moves > moves * 0.25 );
        }
    }

    /**
     * Test Snail movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveSnail() throws Exception
    {
        // Calculate the max depth of a Snail to give it room to avoid turns.
        Snail s = new Snail();
        // The z coordinate determines the scale which determines the size.
        s.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = s.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Snail and make sure it has room to move without
            // reaching an edge. Note: Turns are tested elsewhere.
            s = new Snail();
            s.setZ( Math.max( Math.min( s.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Save the x and y coordinates for testing below, then move it.
            double x = s.getX(), y = s.getY(), z = s.getZ();
            s.move();

            // The Snail should not have moved horizontally.
            assertEquals( "X", x, s.getX(), TOLERANCE );
            // The Snail should have moved vertically, but this is based on z so just check that it moved some.
            assertTrue( "Y", Math.abs( y - s.getY() ) > TOLERANCE );
            // The Snail should have moved forward or backward based on its speed.
            assertEquals( "Z", Math.abs( z - s.getZ() ), Math.abs( s.getSpeed() ), TOLERANCE );
        }
    }

    /**
     * Test Bubble movement.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void moveBubble() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Bubble and make sure it will not reach the top
            // of the aquarium when it moves. Note: The top is tested below.
            Bubble b = new Bubble();
            b.setY( Math.max( b.getY(), b.getHeight() ) );

            // Save the bubble's coordinates for testing below, then move it.
            double x = b.getX(), y = b.getY(), z = b.getZ();
            b.move();

            // The Bubble should not have moved horizontally.
            assertEquals( "X", x, b.getX(), TOLERANCE );
            // The Bubble should have moved up.
            assertTrue( "Y", b.getY() < y );
            // The Bubble should not have moved forward or backward.
            assertEquals( "Z", z, b.getZ(), TOLERANCE );
        }

        // Test that all bubbles move at the same speed.
        Bubble b = new Bubble();
        for( int i = 0; i < N; i++ )
        {
            assertEquals( "S", b.getSpeed(), new Bubble().getSpeed(), TOLERANCE );
        }

        // Test that bubbles move faster than any creatures.
        double fastest = 0.0;
        for( int i = 0; i < N; i++ )
        {
            fastest = Math.max( fastest, Math.abs( new Angelfish().getSpeed() ) );
            fastest = Math.max( fastest, Math.abs( new Clownfish().getSpeed() ) );
            fastest = Math.max( fastest, Math.abs( new Seahorse().getSpeed() ) );
            fastest = Math.max( fastest, Math.abs( new Crab().getSpeed() ) );
            fastest = Math.max( fastest, Math.abs( new Lobster().getSpeed() ) );
            fastest = Math.max( fastest, Math.abs( new Snail().getSpeed() ) );
        }

        assertTrue( "M", Math.abs( b.getSpeed() ) > fastest );
    }

    /**
     * Test Angelfish turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnAngelfish() throws Exception
    {
        // Calculate the max depth of an Angelfish to give it room to turn.
        Angelfish a = new Angelfish();
        // The z coordinate determines the scale which determines the size.
        a.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = a.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Angelfish and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            a = new Angelfish();
            a.setZ( Math.max( Math.min( a.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the left edge, moving left.
            a.setX( -a.getWidth() + 0.5 );
            a.setSpeed( -1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = a.getDepth(), z = a.getZ();
            a.move();

            // The Angelfish should have turned around.
            assertEquals( "T", 1.0, a.getSpeed(), TOLERANCE );
            // The Angelfish should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - a.getZ() ), TOLERANCE );
        }

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Angelfish and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            a = new Angelfish();
            a.setZ( Math.max( Math.min( a.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the right edge, moving right.
            a.setX( Aquarium.WIDTH - 0.5 );
            a.setSpeed( 1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = a.getDepth(), z = a.getZ();
            a.move();

            // The Angelfish should have turned around.
            assertEquals( "T", -1.0, a.getSpeed(), TOLERANCE );
            // The Angelfish should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - a.getZ() ), TOLERANCE );
        }
    }

    /**
     * Test Clownfish turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnClownfish() throws Exception
    {
        // Calculate the max depth of a Clownfish to give it room to turn.
        Clownfish c = new Clownfish();
        // The z coordinate determines the scale which determines the size.
        c.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = c.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Clownfish and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            c = new Clownfish();
            c.setZ( Math.max( Math.min( c.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the left edge, moving left.
            c.setX( -c.getWidth() + 0.5 );
            c.setSpeed( -1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = c.getDepth(), z = c.getZ();
            c.move();

            // The Clownfish should have turned around.
            assertEquals( "T", 1.0, c.getSpeed(), TOLERANCE );
            // The Clownfish should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - c.getZ() ), TOLERANCE );
        }

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Clownfish and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            c = new Clownfish();
            c.setZ( Math.max( Math.min( c.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the right edge, moving right.
            c.setX( Aquarium.WIDTH - 0.5 );
            c.setSpeed( 1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = c.getDepth(), z = c.getZ();
            c.move();

            // The Clownfish should have turned around.
            assertEquals( "T", -1.0, c.getSpeed(), TOLERANCE );
            // The Clownfish should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - c.getZ() ), TOLERANCE );
        }
    }

    /**
     * Test Seahorse turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnSeahorse() throws Exception
    {
        // Calculate the max depth of a Seahorse to give it room to turn.
        Seahorse s = new Seahorse();
        // The z coordinate determines the scale which determines the size.
        s.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxDepth = s.getDepth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Seahorse and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            s = new Seahorse();
            s.setZ( Math.max( Math.min( s.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the left edge, moving left.
            s.setX( -s.getWidth() + 0.5 );
            s.setSpeed( -1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = s.getDepth(), z = s.getZ();
            s.move();

            // The Seahorse should have turned around.
            assertEquals( "T", 1.0, s.getSpeed(), TOLERANCE );
            // The Seahorse should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - s.getZ() ), TOLERANCE );
        }

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Seahorse and place it so there is room for it to move
            // forward or backward in the aquarium when it turns. (Do this first
            // since setting the z coordinate also sets the scale and size!)
            s = new Seahorse();
            s.setZ( Math.max( Math.min( s.getZ(), Aquarium.DEPTH - maxDepth ), maxDepth ) );

            // Now set it one half step from the right edge, moving right.
            s.setX( Aquarium.WIDTH - 0.5 );
            s.setSpeed( 1.0 );

            // Save the depth and z coordinate for testing below, then move it.
            double d = s.getDepth(), z = s.getZ();
            s.move();

            // The Seahorse should have turned around.
            assertEquals( "T", -1.0, s.getSpeed(), TOLERANCE );
            // The Seahorse should have moved forward or backward in the aquarium.
            assertEquals( "Z", d, Math.abs( z - s.getZ() ), TOLERANCE );
        }
    }

    /**
     * Test Crab turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnCrab() throws Exception
    {
        // Calculate the max width of a Crab to give it room to turn.
        Crab c = new Crab();
        // The z coordinate determines the scale which determines the size.
        c.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxWidth = c.getWidth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Crab one-half step from the front edge, moving forward.
            c = new Crab();
            c.setZ( Aquarium.DEPTH - 0.5 );
            c.setSpeed( 1.0 );

            // Also place it so there is room for it to move left or right when it turns.
            c.setX( Math.max( Math.min( c.getX(), Aquarium.WIDTH - maxWidth ), maxWidth ) );

            // Save the x coordinate for testing below, then move it.
            double x = c.getX();
            c.move();

            // The Crab should have turned around.
            assertEquals( "T", -1.0, c.getSpeed(), TOLERANCE );
            // The Crab should have moved left or right in the aquarium.
            assertEquals( "X", c.getWidth(), Math.abs( x - c.getX() ), TOLERANCE );
        }
    }

    /**
     * Test Lobster turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnLobster() throws Exception
    {
        // Calculate the max width of a Lobster to give it room to turn.
        Lobster lob = new Lobster();
        // The z coordinate determines the scale which determines the size.
        lob.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxWidth = lob.getWidth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Lobster one-half step from the front edge, moving forward.
            lob = new Lobster();
            lob.setZ( Aquarium.DEPTH - 0.5 );
            lob.setSpeed( 1.0 );

            // Also place it so there is room for it to move left or right when it turns.
            // Note: Leave extra room from the edges because lobsters also move horizontally.
            lob.setX( Math.max( Math.min( lob.getX(), Aquarium.WIDTH - maxWidth * 2 ), maxWidth * 2 ) );

            // Save the x coordinate for testing below, then move it.
            double x = lob.getX();
            lob.move();

            // The Lobster should have turned around.
            assertEquals( "T", -1.0, lob.getSpeed(), TOLERANCE );
            // The Lobster should have moved left or right in the aquarium.
            // Note: Extra tolerance here because lobsters also move horizontally.
            assertEquals( "X", lob.getWidth(), Math.abs( x - lob.getX() ), TOLERANCE + 1.0 );
        }
    }

    /**
     * Test Snail turns.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnSnail() throws Exception
    {
        // Calculate the max width of a Snail to give it room to turn.
        Snail s = new Snail();
        // The z coordinate determines the scale which determines the size.
        s.setZ( Aquarium.DEPTH );  // All the way forward; largest scale.
        double maxWidth = s.getWidth();

        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Snail one-half step from the front edge, moving forward.
            s = new Snail();
            s.setZ( Aquarium.DEPTH - 0.5 );
            s.setSpeed( 1.0 );

            // Also place it so there is room for it to move left or right when it turns.
            s.setX( Math.max( Math.min( s.getX(), Aquarium.WIDTH - maxWidth ), maxWidth ) );

            // Save the x coordinate for testing below, then move it.
            double x = s.getX();
            s.move();

            // The Snail should have turned around.
            assertEquals( "T", -1.0, s.getSpeed(), TOLERANCE );
            // The Snail should have moved left or right in the aquarium.
            assertEquals( "X", s.getWidth(), Math.abs( x - s.getX() ), TOLERANCE );
        }
    }

    /**
     * Test Bubble turns (actually, that they move from the top to the bottom.)
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void turnBubble() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Bubble, put it just below the top, and move it.
            Bubble b = new Bubble();
            b.setY( 0.1 );
            b.move();

            // The bubble should have moved to the bottom of the aquarium.
            assertTrue( "Y", b.getY() >= Aquarium.HEIGHT - b.getHeight() );
        }
    }

    /**
     * Test Angelfish react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactAngelfish() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Angelfish, make sure it has room to move without
            // reaching an edge, and then save the x coordinate.
            Angelfish a = new Angelfish();
            a.setX( Math.max( Math.min( a.getX(), Aquarium.WIDTH - a.getWidth() ), 0 ) );
            double x = a.getX();

            // Tell the Angelfish to move, react, and move again.
            a.move();
            a.react();
            a.move();

            // The Angelfish should have reversed directions, but kept moving the same speed.
            // Thus, after reacting and moving, its x coordinate should be back where it started.
            assertEquals( "X", x, a.getX(), TOLERANCE );
        }
    }

    /**
     * Test Clownfish react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactClownfish() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Clownfish, make sure it has room to move without
            // reaching an edge, and then save the y coordinate.
            Clownfish c = new Clownfish();
            c.setX( Math.max( Math.min( c.getX(), Aquarium.WIDTH - c.getWidth() ), 0 ) );
            double y1 = c.getY();

            // Tell the Clownfish to move and save the y coordinate again.
            c.move();
            double y2 = c.getY();

            // Tell the Clownfish to react.
            c.react();

            // Tell the Clownfish to move and save the y coordinate a third time.
            c.move();
            double y3 = c.getY();

            // The Clownfish should have not have been moving vertically before
            // reacting and it should have been moving vertically after reacting.
            assertEquals( "Y", y1, y2, TOLERANCE );
            assertNotEquals( "Y", y2, y3, TOLERANCE );
        }
    }

    /**
     * Test Seahorse react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactSeahorse() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Seahorse and save its current location and speed.
            Seahorse s = new Seahorse();
            double x1 = s.getX(), y1 = s.getY(), z1 = s.getZ(), s1 = s.getSpeed();

            // Move the Seahorse and save its location and speed again.
            s.move();
            double x2 = s.getX(), y2 = s.getY(), z2 = s.getZ(), s2 = s.getSpeed();

            // Tell the Seahorse to react.
            s.react();

            // Move the Seahorse and save its location and speed a third time.
            s.move();
            double x3 = s.getX(), y3 = s.getY(), z3 = s.getZ(), s3 = s.getSpeed();

            // Since a Seahorse ignores all other creatures, it should have
            // continued on its merry way the same as it was before reacting.
            // Unless it happened to be on an edge and turn ... ignore those.
            if( s1 * s2 > 0 && s2 * s3 > 0 )
            {
                assertEquals( "X", Math.abs( x1 - x2 ), Math.abs( x2 - x3 ), TOLERANCE );
                assertEquals( "Y", Math.abs( y1 - y2 ), Math.abs( y2 - y3 ), TOLERANCE );
                assertEquals( "Z", Math.abs( z1 - z2 ), Math.abs( z2 - z3 ), TOLERANCE );
            }
        }
    }

    /**
     * Test Crab react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactCrab() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Crab, make sure it will not need to turn when it
            // moves, and make sure it has room to scurry horizontally.
            Crab c = new Crab();
            c.setZ( Math.max( Math.min( c.getZ(), Aquarium.DEPTH - c.getDepth() ), c.getDepth() ) );
            c.setX( Math.max( Math.min( c.getZ(), Aquarium.DEPTH - c.getWidth() ), c.getWidth() ) );

            // Save its x coordinate, and tell it to react.
            double x = c.getX();
            c.react();

            // Artificially set the Crab's delay to 1 so the move will count
            // it down to zero and then the Crab will have a chance to scurry.
            c.setDelay( 1 );
            // Move, which may cause a delay about once every 3-4 seconds.
            c.move();

            // After all that movement, there should have been some horizontal scurrying.
            assertTrue( "X", Math.abs( x - c.getX() ) > TOLERANCE );
        }
    }

    /**
     * Test Lobster react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactLobster() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Lobster, save its location, and tell it to react.
            Lobster lob = new Lobster();
            double x = lob.getX(), y = lob.getY(), z = lob.getZ();
            lob.react();

            // Try moving the lobster for 12 animation frames (a
            // bit less than half a second in a 30fps animation.)
            for( int j = 0; j < 12; j++ )
            {
                lob.move();  // Shouldn't move!
            }

            // The lobster should not move when reacting.
            assertEquals( "X", x, lob.getX(), TOLERANCE );
            assertEquals( "Y", y, lob.getY(), TOLERANCE );
            assertEquals( "Z", z, lob.getZ(), TOLERANCE );
        }
    }

    /**
     * Test Snail react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactSnail() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Snail and save its current location and speed.
            Snail s = new Snail();
            double x1 = s.getX(), y1 = s.getY(), z1 = s.getZ(), s1 = s.getSpeed();

            // Move the Snail and save its location and speed again.
            s.move();
            double x2 = s.getX(), y2 = s.getY(), z2 = s.getZ(), s2 = s.getSpeed();

            // Tell the Snail to react.
            s.react();

            // Move the Snail and save its location and speed a third time.
            s.move();
            double x3 = s.getX(), y3 = s.getY(), z3 = s.getZ(), s3 = s.getSpeed();

            // Since a Snail ignores all other creatures, it should have
            // continued on its merry way the same as it was before reacting.
            // Unless it happened to be on an edge and turn ... ignore those.
            if( s1 * s2 > 0 && s2 * s3 > 0 )
            {
                assertEquals( "X", Math.abs( x1 - x2 ), Math.abs( x2 - x3 ), TOLERANCE );
                assertEquals( "Y", Math.abs( y1 - y2 ), Math.abs( y2 - y3 ), TOLERANCE );
                assertEquals( "Z", Math.abs( z1 - z2 ), Math.abs( z2 - z3 ), TOLERANCE );
            }
        }
    }

    /**
     * Test Bubble react.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void reactBubble() throws Exception
    {
        // There's randomness involved, so repeat the test several times.
        for( int i = 0; i < N; i++ )
        {
            // Create a new Bubble and save its current location and speed.
            Bubble s = new Bubble();
            double x1 = s.getX(), y1 = s.getY(), z1 = s.getZ();

            // Move the Bubble and save its location and speed again.
            s.move();
            double x2 = s.getX(), y2 = s.getY(), z2 = s.getZ();

            // Tell the Bubble to react.
            s.react();

            // Move the Bubble and save its location and speed a third time.
            s.move();
            double x3 = s.getX(), y3 = s.getY(), z3 = s.getZ();

            // Since a Bubble ignores all other creatures, it should have
            // continued on its merry way the same as it was before reacting.
            // Unless it happened to reach the top ... ignore those.
            if( y1 < y2 && y2 < y3 )
            {
                assertEquals( "X", Math.abs( x1 - x2 ), Math.abs( x2 - x3 ), TOLERANCE );
                assertEquals( "Y", Math.abs( y1 - y2 ), Math.abs( y2 - y3 ), TOLERANCE );
                assertEquals( "Z", Math.abs( z1 - z2 ), Math.abs( z2 - z3 ), TOLERANCE );
            }
        }
    }
}
