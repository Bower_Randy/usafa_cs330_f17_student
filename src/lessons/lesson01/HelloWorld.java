package lessons.lesson01;

/**
 * Quintessential first program.
 *
 * @author Everyone
 */
public class HelloWorld
{
    /**
     * Main entry point for the application.
     *
     * @param args Command line arguments; ignored by this application.
     */
    public static void main( String[] args )
    {
        // Displays a simple message in the console window.
        System.out.println( "Hello, World!" );
    }
}
