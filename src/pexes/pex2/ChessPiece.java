package pexes.pex2;

/**
 * This class represents a chess piece used in Shogi Chess. Each chess piece
 * knows the direction it is moving, and its current row and column location.
 * <p>
 * The direction is set to one of the constants ChessBoard.DOWN or ChessBoard.UP.
 * <strong>These constants should be used throughout your code rather than using
 * literal values.</strong>
 * <p>
 * The location is represented as integer values for row and column in the range
 * [1,ChessBoard.SIZE].
 * <p>
 * The location (1,1) is the lower-left corner of the chess board.
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public abstract class ChessPiece
{
    /** Direction this chess piece is moving; ChessBoard.DOWN or ChessBoard.UP. */
    private int direction;
    /** Current row location of this chess piece; (1,1) is the lower-left corner of the board. */
    private int currentRow;
    /** Current column location of this chess piece; (1,1) is the lower-left corner of the board. */
    private int currentCol;

    /**
     * Constructs a ChessPiece object with the given parameters.
     * <p>
     * <strong>Note</strong>: Classes extending ChessPiece must have <strong>exactly one</strong>
     * constructor with <strong>this exact signature</strong>. These constructors will be one-line
     * methods that call the super class constructor, passing along the given parameters:
     * <p>
     * <code>super( direction, row, col );</code>
     *
     * @param direction direction this chess piece is moving; ChessBoard.DOWN or ChessBoard.UP.
     * @param row       row of this chess piece.
     * @param col       column of this chess piece.
     */
    public ChessPiece( int direction, int row, int col )
    {
        this.setDirection( direction );
        this.setLocation( row, col );
    }

    /**
     * Determines if the given (row,col) is a valid move for this chess piece
     * from its current location on this.board; will only be called if this
     * chess piece is <strong><em>NOT</em></strong> captured.
     * <p>
     * Code common to all chess pieces should be placed here and subclasses
     * will need to override this method to add code specific to that piece.
     * <p>
     * All chess pieces must remain on the board and move to a space that
     * is either empty or occupied by an opponent.
     *
     * @param board the ChessBoard on which this piece is attempting to move.
     * @param row   row to which the chess piece is attempting to move.
     * @param col   column to which the chess piece is attempting to move.
     *
     * @return true if (row,col) represents a valid move for this chess piece
     * from its current location on the given board; false otherwise.
     */
    public boolean validMove( ChessBoard board, int row, int col )
    {
        // The (row,col) location must be within the boundaries of the board
        // and either empty or occupied by an opponent.
        return row >= 1 && row <= ChessBoard.SIZE && col >= 1 && col <= ChessBoard.SIZE &&
                (board.getPieceAt( row, col ) == null ||
                        board.getPieceAt( row, col ).getDirection() != this.getDirection());
    }

    /**
     * Determines if the given (row,col) is a valid drop for this chess piece;
     * will only be called if this chess piece <strong><em>IS</em></strong> captured.
     * <p>
     * Code common to all chess pieces should be placed here and subclasses
     * will need to override this method to add code specific to that piece.
     * <p>
     * All chess pieces must be dropped on the board and the location must be empty.
     *
     * @param board the ChessBoard on which this piece is attempting to drop.
     * @param row   row to which the chess piece is attempting to drop.
     * @param col   column to which the chess piece is attempting to drop.
     *
     * @return true if (row,col) represents a valid drop for this chess piece; false otherwise.
     */
    public boolean validDrop( ChessBoard board, int row, int col )
    {
        // The (row,col) location must be within the boundaries of the board and empty.
        return row >= 1 && row <= ChessBoard.SIZE && col >= 1 && col <= ChessBoard.SIZE &&
                board.getPieceAt( row, col ) == null;
    }

    /**
     * Mutator method to set the direction this chess piece is moving.
     * <p>
     * Students do <strong><em>NOT</em></strong> need to modify this method.
     * <p>
     * <strong>Note</strong>: This method should only be called directly by JUnit tests;
     * during actual game play it is only called by the GUI.
     *
     * @param direction direction this chess piece is moving.
     *
     * @throws IllegalArgumentException if direction is not ChessBoard.DOWN or ChessBoard.UP.
     */
    public void setDirection( int direction )
    {
        //  _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
        // |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
        // | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
        // | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
        // | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
        // |_____/_\____/_ |_|_\_|\____/  |_| __ |_|__|_|\____/|_____/_____|_|_____  |_|
        // |__   __| |  | |_   _|/ ____| |  \/  |  ____|__   __| |  | |/ __ \|  __ \
        //    | |  | |__| | | | | (___   | \  / | |__     | |  | |__| | |  | | |  | |
        //    | |  |  __  | | |  \___ \  | |\/| |  __|    | |  |  __  | |  | | |  | |
        //    | |  | |  | |_| |_ ____) | | |  | | |____   | |  | |  | | |__| | |__| |
        //    |_|  |_|  |_|_____|_____/  |_|  |_|______|  |_|  |_|  |_|\____/|_____/
        //
        // A subclass may override this method, but the first line must be a call to
        // super.setLocation( row, col ) to ensure this code is executed.

        if( direction != ChessBoard.DOWN && direction != ChessBoard.UP )
        {
            throw new IllegalArgumentException( "ERROR: Direction must be ChessBoard.DOWN or ChessBoard.UP." );
        }
        this.direction = direction;
    }

    /**
     * Mutator method to set the location of this chess piece to the indicated row,col.
     * <p>
     * Students do <strong><em>NOT</em></strong> need to modify this method.
     * <p>
     * <strong>Note</strong>: This method should only be called directly by JUnit tests;
     * during actual game play it is only called by the GUI.
     *
     * @param row row of this chess piece.
     * @param col column of this chess piece.
     *
     * @throws IllegalArgumentException if either row or col is not in range [1,ChessBoard.SIZE].
     */
    public void setLocation( int row, int col )
    {
        //  _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
        // |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
        // | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
        // | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
        // | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
        // |_____/_\____/_ |_|_\_|\____/  |_| __ |_|__|_|\____/|_____/_____|_|_____  |_|
        // |__   __| |  | |_   _|/ ____| |  \/  |  ____|__   __| |  | |/ __ \|  __ \
        //    | |  | |__| | | | | (___   | \  / | |__     | |  | |__| | |  | | |  | |
        //    | |  |  __  | | |  \___ \  | |\/| |  __|    | |  |  __  | |  | | |  | |
        //    | |  | |  | |_| |_ ____) | | |  | | |____   | |  | |  | | |__| | |__| |
        //    |_|  |_|  |_|_____|_____/  |_|  |_|______|  |_|  |_|  |_|\____/|_____/
        //
        // A subclass may override this method, but the first line must be a call to
        // super.setLocation( row, col ) to ensure this code is executed.

        if( row < 1 || row > ChessBoard.SIZE )
        {
            throw new IllegalArgumentException( "ERROR: Row out of range." );
        }

        if( col < 1 || col > ChessBoard.SIZE )
        {
            throw new IllegalArgumentException( "ERROR: Col out of range." );
        }

        this.currentRow = row;
        this.currentCol = col;
    }

    /**
     * Accessor method for current direction.
     * <p>
     * Students do <strong><em>NOT</em></strong> need to modify this method.
     *
     * @return current direction of this chess piece.
     */
    public int getDirection()
    {
        //  _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
        // |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
        // | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
        // | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
        // | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
        // |_____/_\____/_ |_|_\_|\____/  |_| __ |_|__|_|\____/|_____/_____|_|_____  |_|
        // |__   __| |  | |_   _|/ ____| |  \/  |  ____|__   __| |  | |/ __ \|  __ \
        //    | |  | |__| | | | | (___   | \  / | |__     | |  | |__| | |  | | |  | |
        //    | |  |  __  | | |  \___ \  | |\/| |  __|    | |  |  __  | |  | | |  | |
        //    | |  | |  | |_| |_ ____) | | |  | | |____   | |  | |  | | |__| | |__| |
        //    |_|  |_|  |_|_____|_____/  |_|  |_|______|  |_|  |_|  |_|\____/|_____/

        return this.direction;
    }

    /**
     * Accessor method for current row.
     * <p>
     * Students do <strong><em>NOT</em></strong> need to modify this method.
     *
     * @return current row of this chess piece.
     */
    public int getCurrentRow()
    {
        //  _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
        // |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
        // | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
        // | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
        // | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
        // |_____/_\____/_ |_|_\_|\____/  |_| __ |_|__|_|\____/|_____/_____|_|_____  |_|
        // |__   __| |  | |_   _|/ ____| |  \/  |  ____|__   __| |  | |/ __ \|  __ \
        //    | |  | |__| | | | | (___   | \  / | |__     | |  | |__| | |  | | |  | |
        //    | |  |  __  | | |  \___ \  | |\/| |  __|    | |  |  __  | |  | | |  | |
        //    | |  | |  | |_| |_ ____) | | |  | | |____   | |  | |  | | |__| | |__| |
        //    |_|  |_|  |_|_____|_____/  |_|  |_|______|  |_|  |_|  |_|\____/|_____/

        return this.currentRow;
    }

    /**
     * Accessor method for current column.
     * <p>
     * Students do <strong><em>NOT</em></strong> need to modify this method.
     *
     * @return current column of this chess piece.
     */
    public int getCurrentCol()
    {
        //  _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
        // |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
        // | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
        // | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
        // | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
        // |_____/_\____/_ |_|_\_|\____/  |_| __ |_|__|_|\____/|_____/_____|_|_____  |_|
        // |__   __| |  | |_   _|/ ____| |  \/  |  ____|__   __| |  | |/ __ \|  __ \
        //    | |  | |__| | | | | (___   | \  / | |__     | |  | |__| | |  | | |  | |
        //    | |  |  __  | | |  \___ \  | |\/| |  __|    | |  |  __  | |  | | |  | |
        //    | |  | |  | |_| |_ ____) | | |  | | |____   | |  | |  | | |__| | |__| |
        //    |_|  |_|  |_|_____|_____/  |_|  |_|______|  |_|  |_|  |_|\____/|_____/

        return this.currentCol;
    }
}
