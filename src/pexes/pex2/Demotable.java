package pexes.pex2;

/**
 * Implementing this interface indicates a chess piece can be demoted.
 * <p><pre>
 *   _____   ____    _   _  ____ _______   __  __  ____  _____ _____ ________     __
 *  |  __ \ / __ \  | \ | |/ __ \__   __| |  \/  |/ __ \|  __ \_   _|  ____\ \   / /
 *  | |  | | |  | | |  \| | |  | | | |    | \  / | |  | | |  | || | | |__   \ \_/ /
 *  | |  | | |  | | | . ` | |  | | | |    | |\/| | |  | | |  | || | |  __|   \   /
 *  | |__| | |__| | | |\  | |__| | | |    | |  | | |__| | |__| || |_| |       | |
 *  |_____/_\____/  |_|_\_|\____/__|_| ___|_|_ |_|\____/|_____/_____|_|__     |_|
 *     |__   __| |  | |_   _|/ ____|  / ____| |        /\    / ____/ ____|
 *        | |  | |__| | | | | (___   | |    | |       /  \  | (___| (___
 *        | |  |  __  | | |  \___ \  | |    | |      / /\ \  \___ \\___ \
 *        | |  | |  | |_| |_ ____) | | |____| |____ / ____ \ ____) |___) |
 *        |_|  |_|  |_|_____|_____/   \_____|______/_/    \_\_____/_____/
 * </pre>
 *
 * @author Dr. Randy Bower
 */
public abstract interface Demotable
{
    /**
     * Creates and returns the demoted version of this chess piece.
     *
     * @return demoted version of this chess piece.
     */
    public abstract ChessPiece demote();
}
