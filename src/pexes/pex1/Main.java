package pexes.pex1;

/**
 * Main class with a static main method to launch the application.
 * <p>
 * <strong>Documentation</strong>: _YOUR_DETAILED_DOCUMENTATION_STATEMENT_HERE_.
 * </p>
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Main
{
    /**
     * The main method creates a new Aquarium object.
     *
     * @param args Command line arguments; ignored by this application.
     */
    public static void main( String[] args )
    {
        new Aquarium();
    }
}
