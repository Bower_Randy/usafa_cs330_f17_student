package pexes.pex1;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Bubbles exist in the aquarium and float straight up, all
 * moving at the same speed, faster than all other creatures.
 * <p>
 * Bubbles are not technically living creatures, but given the
 * similarity of information in a basic creature and a bubble,
 * it should be convenient to make this a subclass of Creature.
 * </p>
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Bubble extends Creature
{
    // **** NOTE: For this programming exercise, all class attributes
    // **** for all creatures must be declared as private.

    /** Image to be drawn on the aquarium for this bubble. */
    private BufferedImage image;
    /** Current x position of this bubble. */
    private double x;
    /** Current y position of this bubble. */
    private double y;
    /** Current z position of this bubble. */
    private double z;
    /**
     * The scale of a bubble. This is determined by its distance from the
     * front of the aquarium; see note in Aquarium about Aquarium.DEPTH.
     */
    private double scale;
    /**
     * Speed this bubble is moving. Positive values indicate left-to-right,
     * top-to-bottom, or back-to-front; negative values indicate the opposite.
     * Absolute value of speed must be between 0.5 and 2.0.
     */
    private double speed;

    /**
     * Constructor creates a new Bubble with properties set to random
     * values within the bounds of the aquarium, but the speed of all
     * bubbles is the same.
     */
    public Bubble()
    {
        // Use the class name to load the image file.
        this.image = Aquarium.loadImage( this.getClass().getSimpleName() );

        // The x, y, and z coordinates are random within the aquarium
        // with (0,0,0) being the top, left, rear corner.
        this.x = Math.random() * Aquarium.WIDTH;
        this.y = Math.random() * Aquarium.HEIGHT;
        this.z = Math.random() * Aquarium.DEPTH;

        // Scale depends on the z coordinate. See comment above.
        this.scale = 0.5 + this.z / Aquarium.DEPTH / 2.0;

        // All bubbles move up at the same speed.
        this.speed = -2.0;
    }

    /**
     * Bubbles move straight up and start over at a random location
     * on the bottom of the aquarium when the reach the top.
     */
    public void move()
    {
        // Bubbles move straight up, faster than anything else.
        this.setY( this.getY() + this.getSpeed() * 2.0 );

        // When a bubble reaches the top, reset it to a random location
        // at the bottom of the aquarium.
        if( this.getY() <= 0 )
        {
            this.setX( Math.random() * Aquarium.WIDTH );
            this.setY( Aquarium.HEIGHT ); // Bottom of the aquarium.
            this.setZ( Math.random() * Aquarium.DEPTH );
        }
    }

    /**
     * Paints the bubble image with the given graphics object.
     *
     * @param g The Graphics object to use to paint this bubble.
     */
    public void paint( Graphics g )
    {
        // The bubble image is symmetrical, so it doesn't matter which way it is facing.
        g.drawImage( this.image, (int) x, (int) y,
                (int) (x + this.image.getWidth() * this.scale),
                (int) (y + this.image.getHeight() * this.scale),
                0, 0, this.image.getWidth(), this.image.getHeight(), null );
    }

    /**
     * Sets the x position of this bubble, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * bubble. Bubbles are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the bubble as well as the aquarium's dimensions.
     *
     * @param x New x position for this bubble.
     */
    public void setX( double x )
    {
        // Can go off the right and left edges of the aquarium.
        this.x = Math.max( Math.min( x, Aquarium.WIDTH ), -this.getWidth() );
    }

    /**
     * Sets the y position of this bubble, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * bubble. Bubbles are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the bubble as well as the aquarium's dimensions.
     *
     * @param y New y position for this bubble.
     */
    public void setY( double y )
    {
        // Cannot go off the top edge, can go below the bottom edge.
        this.y = Math.max( Math.min( y, Aquarium.HEIGHT ), 0 );
    }

    /**
     * Sets the z position of this bubble, ensuring the value is valid.
     * The (x,y,z) position identifies the upper-left-rear corner of the
     * bubble. Bubbles are allowed to move off the left, right, and
     * bottom edges of the aquarium, so the validity check must consider
     * the dimensions of the bubble as well as the aquarium's dimensions.
     * <p>
     * <strong>Note</strong>: The depth of the aquarium is the distance
     * from the back to the front of the aquarium, not the water depth.
     * </p>
     *
     * @param z New z position for this bubble.
     */
    public void setZ( double z )
    {
        // Cannot go off the back or the front of the aquarium.
        this.z = Math.max( Math.min( z, Aquarium.DEPTH ), 0 );

        // A bubble's scale is determined by its z position. That is, the
        // perceived size depends on how far it is from the front the aquarium.
        this.scale = 0.5 + this.z / Aquarium.DEPTH / 2.0;
    }

    /**
     * Sets the speed of this bubble, which must be between 0.5 and 2.0,
     * positive or negative.
     *
     * @param speed New speed position for this bubble.
     *
     * @throws IllegalArgumentException If speed is out of range.
     */
    public void setSpeed( double speed )
    {
        if( Math.abs( speed ) < 0.5 || Math.abs( speed ) > 2.0 )
        {
            throw new IllegalArgumentException( "speed is out of range: " + speed );
        }
        this.speed = speed;
    }

    /**
     * Convenience method to set the location of this creature.
     *
     * @param x     New x position for this creature.
     * @param y     New y position for this creature.
     * @param z     New z position for this creature.
     */
    public void setXYZ( double x, double y, double z )
    {
        // Must set z first as this also sets scale which determines
        // width and height which are used in both setY and setX.
        this.setZ( z );
        this.setY( y );
        this.setX( x );
    }

    /**
     * Accessor method for the x position of this bubble.
     *
     * @return The current x position of this bubble.
     */
    public double getX()
    {
        return this.x;
    }

    /**
     * Accessor method for the y position of this bubble.
     *
     * @return The current y position of this bubble.
     */
    public double getY()
    {
        return this.y;
    }

    /**
     * Accessor method for the z position of this bubble.
     *
     * @return The current z position of this bubble.
     */
    public double getZ()
    {
        return this.z;
    }

    /**
     * Accessor method for the speed of this bubble.
     *
     * @return The current speed of this bubble.
     */
    public double getSpeed()
    {
        return this.speed;
    }

    /**
     * Accessor method for the scale of this bubble.
     *
     * @return The current scale of this bubble.
     */
    public double getScale()
    {
        return this.scale;
    }

    /**
     * Accessor method for the width of this bubble. It is determined by
     * the current scale of the bubble and the width of the bubble's image.
     *
     * @return The current width of this bubble.
     */
    public double getWidth()
    {
        return this.image.getWidth() * this.scale;
    }

    /**
     * Accessor method for the height of this bubble. It is determined by
     * the current scale of the bubble and the height of the bubble's image.
     *
     * @return The current height of this bubble.
     */
    public double getHeight()
    {
        return this.image.getHeight() * this.scale;
    }

    /**
     * Accessor method for the depth of this bubble. It is determined by
     * the current scale of the bubble and is somewhat arbitrarily set
     * to one-half of the height of the bubble.
     * <p>
     * <strong>Note</strong>: The depth of a bubble is its "thickness"
     * or the amount of space it occupies front to back in the aquarium.
     * </p>
     *
     * @return The current depth of this bubble.
     */
    public double getDepth()
    {
        return this.getHeight() / 2;
    }

    /**
     * Creates and returns a string representation of this bubble that
     * includes the position, size, scale, and speed of the bubble.
     * Only used for debugging, so it's not very fancy.
     *
     * @return A string representation of this bubble.
     */
    @Override
    public String toString()
    {
        return String.format( "%s (%.1f, %.1f, %.1f), (%.1f, %.1f, %.1f), %.1f, %.1f",
                this.getClass().getSimpleName(), this.getX(), this.getY(), this.getZ(),
                this.getWidth(), this.getHeight(), this.getDepth(), this.getScale(), this.getSpeed() );
    }
}
