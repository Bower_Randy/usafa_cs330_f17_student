package pexes.pex1;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * A virtual aquarium with Angelfish, Clownfish, Crabs, Lobsters, Seahorses, and Snails.
 * <p>
 * <strong>Documentation</strong>: This code is original;
 * virtual aquarium inspiration comes from Dr. Schweitzer.
 * </p>
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class Aquarium
{
    /**
     * The dimensions here are the dimensions of a "window" into an aquarium.
     * Creatures are allowed to swim or crawl outside the bounds of this window
     * as if they were just out of view of the window, but will later return.
     * <p>
     * <strong>Note</strong>: The coordinate <code>(0,0,0)</code> is the
     * top, left, rear corner of the aquarium.
     * </p>
     */
    public static final int WIDTH = 1024;
    /**
     * The height of the aquarium is based on width to produce the visually
     * appealing 4:3 ratio. For best results, set WIDTH to 800, 1024 or 1200.
     * <p>
     * <strong>Note</strong>: The coordinate <code>(0,0,0)</code> is the
     * top, left, rear corner of the aquarium.
     * </p>
     */
    public static final int HEIGHT = WIDTH * 3 / 4;
    /**
     * The depth of the aquarium is the visual depth, front to back, not water
     * depth from the top; rather arbitrarily set to one-half of the height.
     * <p>
     * This visual depth, front to back, is realized by setting the scale of
     * each creature in the aquarium such that creatures closer to the front
     * are larger than creatures closer to the back.
     * </p>
     * <p>
     * <strong>Note</strong>: The coordinate <code>(0,0,0)</code> is the
     * top, left, rear corner of the aquarium.
     * </p>
     */
    public static final int DEPTH = HEIGHT / 2;

    /**
     * The constructor creates a new aquarium and starts a timer that triggers
     * the AquariumPanel's actionPerformed method which controls the animation.
     */
    public Aquarium()
    {
        // Create the frame with the title "Aquarium".
        JFrame aquariumFrame = new JFrame( "Aquarium" );
        aquariumFrame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );

        // Set the location of the frame slightly offset from the
        // upper-left corner of the screen.
        aquariumFrame.setLocation( 16, 16 );

        // Set the size of the frame with a few extra pixels for the border and
        // title bar so the panel inside the frame is exactly WIDTH x HEIGHT.
        aquariumFrame.setSize( WIDTH + 16, HEIGHT + 38 );

        // Create the aquarium panel and add it to the frame.
        AquariumPanel aquariumPanel = new AquariumPanel();
        aquariumFrame.add( aquariumPanel );

        // Make the frame visible.
        aquariumFrame.setVisible( true );

        // Create and start a timer that triggers the actionPerformed
        // method in the aquarium panel every 33 milliseconds for a
        // roughly 30 frames-per-second animation.
        new Timer( 33, aquariumPanel ).start();
    }

    /**
     * This method loads an image with the given file name from the images
     * folder of the project.
     *
     * @param name File name of the image.
     *
     * @return BufferedImage object representing the image;
     * null if there was an error loading the image.
     */
    static BufferedImage loadImage( String name )
    {
        BufferedImage image = null;
        try
        {
            image = ImageIO.read( new File( "./images/" + name + ".png" ) );
        }
        catch( IOException e )
        {
            System.err.println( "Error loading image: " + name );
            System.exit( 1 );
        }
        return image;
    }

    /**
     * Inner class extending JPanel to inherit the paint functionality.
     * This class also implements the ActionListener interface so it
     * can be used with a Timer to create the animation.
     */
    private class AquariumPanel extends JPanel implements ActionListener
    {
        /** The background image displayed in the aquarium. */
        private final BufferedImage background = loadImage( "Background" );
        /** How many of each aquatic creature in the aquarium. */
        private final int N = 3;

        // TODO: Create a class named Creature that is a parent class
        // TODO: for all aquatic creatures in the aquarium and use it
        // TODO: to consolidate the following arrays into one array.

        /** ArrayList of bubbles contained in the aquarium. */
        private ArrayList<Bubble> bubbles;
        /** ArrayList of angelfish contained in the aquarium. */
        private ArrayList<Angelfish> angelfish;
        /** ArrayList of clownfish contained in the aquarium. */
        private ArrayList<Clownfish> clownfish;
        /** ArrayList of seahorses contained in the aquarium. */
        private ArrayList<Seahorse> seahorses;
        /** ArrayList of crabs contained in the aquarium. */
        private ArrayList<Crab> crabs;
        /** ArrayList of lobsters contained in the aquarium. */
        private ArrayList<Lobster> lobsters;
        /** ArrayList of snails contained in the aquarium. */
        private ArrayList<Snail> snails;

        /** Constructor creates all of the creatures in the aquarium. */
        public AquariumPanel()
        {
            // Call the superclass constructor to create the JPanel.
            super();

            // Create the ArrayLists to hold creature references.
            this.angelfish = new ArrayList<>();
            this.clownfish = new ArrayList<>();
            this.seahorses = new ArrayList<>();
            this.crabs = new ArrayList<>();
            this.lobsters = new ArrayList<>();
            this.snails = new ArrayList<>();

            // Create the actual creature objects in the aquarium.
            for( int i = 0; i < N; i++ )
            {
                this.angelfish.add( new Angelfish() );
                this.clownfish.add( new Clownfish() );
                this.seahorses.add( new Seahorse() );
                this.crabs.add( new Crab() );
                this.lobsters.add( new Lobster() );
                this.snails.add( new Snail() );
            }

            // Two bubbles for each creature.
            this.bubbles = new ArrayList<>();
            for( int i = 0; i < N * 6 * 2; i++ )
            {
                this.bubbles.add( new Bubble() );
            }
        }

        /**
         * The actionPerformed event is fired by the timer created in the Aquarium constructor.
         *
         * @param ae ActionEvent object associated with this event; not used in this application.
         */
        @Override
        public void actionPerformed( ActionEvent ae )
        {
            // TODO: It should be easier to move all creatures/bubbles with everything on one array!

            // First, move each of the creatures in the aquarium, including bubbles.
            for( Angelfish a : this.angelfish )  a.move();
            for( Clownfish c : this.clownfish )  c.move();
            for( Seahorse s : this.seahorses )  s.move();
            for( Crab c : this.crabs )  c.move();
            for( Lobster l : this.lobsters )  l.move();
            for( Snail s : this.snails )  s.move();
            for( Bubble b : this.bubbles )  b.move();

            // TODO: The body of these nested for loops should be a whole lot shorter as well!

            // Now see if any of the newly moved creatures have met another creature.
            for( int i = 0; i < N; i++ )
            {
                for( int j = 0; j < N; j++ )
                {
                    if( i != j && this.angelfish.get( i ).met( this.angelfish.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        this.angelfish.get( j ).react();
                    }

                    if( this.angelfish.get( i ).met( this.clownfish.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        this.clownfish.get( j ).react();
                    }

                    if( this.angelfish.get( i ).met( this.seahorses.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        // Seahorses do nothing when they react another creature.
                    }

                    if( this.angelfish.get( i ).met( this.crabs.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        this.crabs.get( j ).react();
                    }

                    if( this.angelfish.get( i ).met( this.lobsters.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        this.lobsters.get( j ).react();
                    }

                    if( this.angelfish.get( i ).met( this.snails.get( j ) ) )
                    {
                        this.angelfish.get( i ).react();
                        // Snails do nothing when they react another creature.
                    }

                    if( i != j && this.clownfish.get( i ).met( this.clownfish.get( j ) ) )
                    {
                        this.clownfish.get( i ).react();
                        this.clownfish.get( j ).react();
                    }

                    if( this.clownfish.get( i ).met( this.seahorses.get( j ) ) )
                    {
                        this.clownfish.get( i ).react();
                        // Seahorses do nothing when they react another creature.
                    }

                    if( this.clownfish.get( i ).met( this.crabs.get( j ) ) )
                    {
                        this.clownfish.get( i ).react();
                        this.crabs.get( j ).react();
                    }

                    if( this.clownfish.get( i ).met( this.lobsters.get( j ) ) )
                    {
                        this.clownfish.get( i ).react();
                        this.lobsters.get( j ).react();
                    }

                    if( this.clownfish.get( i ).met( this.snails.get( j ) ) )
                    {
                        this.clownfish.get( i ).react();
                        // Snails do nothing when they react another creature.
                    }

                    if( this.seahorses.get( i ).met( this.crabs.get( j ) ) )
                    {
                        // Seahorses do nothing when they react another creature.
                        this.crabs.get( j ).react();
                    }

                    if( this.seahorses.get( i ).met( this.lobsters.get( j ) ) )
                    {
                        // Seahorses do nothing when they react another creature.
                        this.lobsters.get( j ).react();
                    }

                    if( i != j && this.crabs.get( i ).met( this.crabs.get( j ) ) )
                    {
                        this.crabs.get( i ).react();
                        this.crabs.get( j ).react();
                    }

                    if( this.crabs.get( i ).met( this.lobsters.get( j ) ) )
                    {
                        this.crabs.get( i ).react();
                        this.lobsters.get( j ).react();
                    }

                    if( this.crabs.get( i ).met( this.snails.get( j ) ) )
                    {
                        this.crabs.get( i ).react();
                        // Snails do nothing when they react another creature.
                    }

                    if( i != j && this.lobsters.get( i ).met( this.lobsters.get( j ) ) )
                    {
                        this.lobsters.get( i ).react();
                        this.lobsters.get( j ).react();
                    }

                    if( this.lobsters.get( i ).met( this.snails.get( j ) ) )
                    {
                        this.lobsters.get( i ).react();
                        // Snails do nothing when they react another creature.
                    }
                }
            }

            // TODO: Consolidating all creatures into one array will solve this problem!

            /*
             * PROBLEM: With all creatures stored in separate arrays, it is easy
             * to sort Angelfish with respect to other Angelfish, but how do we
             * sort Angelfish with respect to Clownfish, for example?
             */

            // Sort the creatures so some will appear "behind" others.
            // See comment on the sort method below.
            sortBubbles();
            sortAngelfish();
            sortClownfish();
            sortSeahorses();
            sortCrabs();
            sortLobsters();
            sortSnails();

            // NOTE: Future exercise ... use Comparable or Comparator to sort.

            // Finally, after all creatures have moved and all meetings are
            // taken care of, repaint the entire aquarium. NOTE: None of the
            // above code tried to paint anything!! Everything is moved at
            // once and only then is the entire aquarium repainted.
            repaint();
        }

        // TODO: Will we need this many separate sort methods? I think not!

        /**
         * This method sorts the array of bubbles so those with the smallest
         * z positions are first. This means when the bubbles are drawn, the
         * bubbles with smaller z values (closer to the back of the tank)
         * will appear to be behind the other bubbles when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortBubbles()
        {
            Bubble temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.bubbles.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.bubbles.size(); j++ )
                {
                    if( this.bubbles.get( j ).getZ() < this.bubbles.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.bubbles.get( i );
                this.bubbles.set( i, this.bubbles.get( smallest ) );
                this.bubbles.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortAngelfish()
        {
            Angelfish temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.angelfish.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.angelfish.size(); j++ )
                {
                    if( this.angelfish.get( j ).getZ() < this.angelfish.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.angelfish.get( i );
                this.angelfish.set( i, this.angelfish.get( smallest ) );
                this.angelfish.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortClownfish()
        {
            Clownfish temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.clownfish.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.clownfish.size(); j++ )
                {
                    if( this.clownfish.get( j ).getZ() < this.clownfish.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.clownfish.get( i );
                this.clownfish.set( i, this.clownfish.get( smallest ) );
                this.clownfish.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortSeahorses()
        {
            Seahorse temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.seahorses.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.seahorses.size(); j++ )
                {
                    if( this.seahorses.get( j ).getZ() < this.seahorses.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.seahorses.get( i );
                this.seahorses.set( i, this.seahorses.get( smallest ) );
                this.seahorses.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortCrabs()
        {
            Crab temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.crabs.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.crabs.size(); j++ )
                {
                    if( this.crabs.get( j ).getZ() < this.crabs.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.crabs.get( i );
                this.crabs.set( i, this.crabs.get( smallest ) );
                this.crabs.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortLobsters()
        {
            Lobster temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.lobsters.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.lobsters.size(); j++ )
                {
                    if( this.lobsters.get( j ).getZ() < this.lobsters.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.lobsters.get( i );
                this.lobsters.set( i, this.lobsters.get( smallest ) );
                this.lobsters.set( smallest, temp );
            }
        }

        /**
         * This method sorts an array of creatures so those with the smallest
         * z positions are first. This means when the creatures are drawn, the
         * creatures with smaller z values (closer to the back of the tank)
         * will appear to be behind the other creatures when they overlap.
         * The method uses a simple selection sort algorithm.
         */
        private void sortSnails()
        {
            Snail temp; // Temporary variable to use when swapping.
            for( int i = 0; i < this.snails.size() - 1; i++ )
            {
                int smallest = i;
                for( int j = i + 1; j < this.snails.size(); j++ )
                {
                    if( this.snails.get( j ).getZ() < this.snails.get( smallest ).getZ() )
                    {
                        smallest = j;
                    }
                }

                temp = this.snails.get( i );
                this.snails.set( i, this.snails.get( smallest ) );
                this.snails.set( smallest, temp );
            }
        }

        /**
         * This method paints the aquarium.
         *
         * @param g The Graphics object to use to paint.
         */
        @Override
        public void paintComponent( Graphics g )
        {
            // Make sure the JPanel does whatever it needs to do when painted.
            super.paintComponent( g );

            // Draw the background image.
            g.drawImage( this.background, 0, 0,
                    this.getWidth(), this.getHeight(), null );

            // TODO: This code will also be shorter with everything in one array.

            // Paint all the bubbles.
            for( Bubble bubble : this.bubbles )
            {
                bubble.paint( g );
            }

            // Paint all the aquatic creatures.
            for( Angelfish a : this.angelfish )  a.paint( g );
            for( Clownfish c : this.clownfish )  c.paint( g );
            for( Seahorse s : this.seahorses )  s.paint( g );
            for( Crab c : this.crabs )  c.paint( g );
            for( Lobster l : this.lobsters )  l.paint( g );
            for( Snail s : this.snails )  s.paint( g );
            for( Bubble b : this.bubbles )  b.paint( g );
        }
    }
}
