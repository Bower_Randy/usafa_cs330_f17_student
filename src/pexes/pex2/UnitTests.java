package pexes.pex2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Chess piece unit tests.
 *
 * @author Dr. Randy Bower, _YOUR_NAME_HERE_
 */
public class UnitTests
{
    /** An empty chess board for testing. */
    private ChessBoard emptyBoard;

    /**
     * This method runs before each individual test.
     */
    @Before
    public void setUp()
    {
        // Just call the constructor, without init, to create the empty board.
        this.emptyBoard = new ChessBoard();
    }

    /**
     * Test the Pawn's validMove method.
     *
     * @throws Exception If there is an exception; duh.
     */
    @Test
    public void pawnMoves() throws Exception
    {
        // NOTE: This is just a sample to get you started; it is
        // not intended to be a complete test for the Pawn class!

        // Test a valid move for a Pawn moving up.
        ChessPiece p = new Pawn( ChessBoard.UP, 5, 5 );
        assertTrue( p.validMove( this.emptyBoard, p.getCurrentRow() + p.getDirection(), p.getCurrentCol() ) );

        // Test a valid move for a Pawn moving down.
        p.setDirection( ChessBoard.DOWN );
        assertTrue( p.validMove( this.emptyBoard, p.getCurrentRow() + p.getDirection(), p.getCurrentCol() ) );
    }
}
